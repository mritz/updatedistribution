﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.AssemblySigning
{
    internal class AssemblySigner
    {
        private static readonly object Lock = new object();

        private readonly string _directory;

        private string _signScript;

        public AssemblySigner(string directory)
        {
            _directory = directory;
        }

        private string SignScript
        {
            get { return _signScript ?? (_signScript = File.ReadAllText(@".\Script.txt")); }
        }

        public SigningPackageResult Sign()
        {
            var filesToSign = new List<string>();
            filesToSign.AddRange(Directory.GetFiles(_directory, "*.dll", SearchOption.AllDirectories).ToList());
            filesToSign.AddRange(Directory.GetFiles(_directory, "*.exe", SearchOption.AllDirectories).ToList());

            if(!filesToSign.Any())
                return SigningPackageResult.NoSigningRequired;

            try
            {
                foreach (var fileToSign in filesToSign)
                {
                    var versionInfo = FileVersionInfo.GetVersionInfo(fileToSign);
                    if ((versionInfo.CompanyName != null) &&
                        versionInfo.CompanyName.Equals("TaxSlayer LLC", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!RunSigningScript(fileToSign))
                        {
                            throw new Exception(string.Format("Unable to sign {0}", fileToSign));
                        }
                        Logger.Info(string.Format("{0} was signed at {1}", fileToSign, DateTime.Now));
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return SigningPackageResult.Error;
            }

            return SigningPackageResult.DirectorySigned;
        }

        private bool RunSigningScript(string fileToSign)
        {
            var powerShell = PowerShell.Create();
            var log = new StringBuilder();

            powerShell.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted", true);
            powerShell.AddScript(SignScript, true);
            powerShell.AddParameter("p", fileToSign);

            Collection<PSObject> results;

            lock (Lock)
            {
                results = powerShell.Invoke();
            }

            var errors = powerShell.Streams.Error.ReadAll();
            if (errors.Any())
            {
                foreach (var error in errors)
                {
                    log.AppendLine(error.ToString());
                }
                Logger.Error(log.ToString());
                return false;
            }

            foreach (var result in results)
            {
                var signature = result.BaseObject as Signature;
                if (signature != null)
                {
                    if (signature.Status == SignatureStatus.Valid)
                    {
                        return true;
                    }
                    Logger.Info(
                        string.Format(
                            "Error signing {0}, Status {1}, Message {2}",
                            fileToSign,
                            signature.Status,
                            signature.StatusMessage));
                }
                log.AppendLine(result.ToString());
            }
            return false;
        }
    }

    internal enum SigningPackageResult
    {
        NoSigningRequired,
        DirectorySigned,
        Error
    }
}