﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;

using RCS.Common.Compression;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Models;
using UpdateDistributor.Core.Rest;

namespace UpdateDistributor.AssemblySigning
{
    internal class Program
    {
        private static RestClient _client;

        private static void Main()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var restApiUrl = ConfigurationManager.AppSettings["UpdateDistributorRestApi"];
            _client = new RestClient(restApiUrl);

            var menuOptions = new Dictionary<int, string>();
            var taxYears = _client.GetAsync<IEnumerable<string>>("TaxYear", null).Result.ToList();

            for (var i = 0; i < taxYears.Count; i++)
            {
                menuOptions.Add(i + 1, taxYears[i]);
            }

            int selectedOption;
            do
            {
                Console.Clear();
                Console.WriteLine("Select a tax year to sign");
                foreach (var menuOption in menuOptions)
                {
                    Console.WriteLine("{0}. {1}", menuOption.Key, menuOption.Value);
                }
                Console.WriteLine("0. Exit");
                Console.Write("Select: ");
                if (!int.TryParse(Console.ReadLine(), out selectedOption))
                {
                    Console.WriteLine("Do you see an option \"{0}\"? Try again, dumbass", selectedOption);
                    selectedOption = -1;
                }
                if (selectedOption > 0)
                {
                    SignPackagesForTaxYear(menuOptions[selectedOption]);
                    Console.WriteLine("Press any key...");
                    Console.ReadLine();
                }
            } while (selectedOption != 0);
        }
        private static void SignPackagesForTaxYear(string taxYear)
        {
            var stageRootDirectory = Path.Combine(Path.GetTempPath(), "Signing Stage");

            if (Directory.Exists(stageRootDirectory))
            {
                Directory.Delete(stageRootDirectory, true);
            }
            Directory.CreateDirectory(stageRootDirectory);

            var signingPackages =
                _client.GetAsync<IEnumerable<SigningPackage>>("SigningPackage", null, taxYear).Result.ToList();

            Console.WriteLine("Retrieved {0} pending signing packages for tax year {1}", signingPackages.Count, taxYear);
            foreach (var signingPackage in signingPackages)
            {
                var packageDirectory = Path.Combine(
                    stageRootDirectory,
                    signingPackage.TaxYear,
                    signingPackage.Distribution.ParcelId.ToString());
                if (!Directory.Exists(packageDirectory))
                {
                    Directory.CreateDirectory(packageDirectory);
                }

                var zipFile = Path.Combine(packageDirectory, "package.zip");
                File.WriteAllBytes(zipFile, signingPackage.ZipBytes);
                var zipper = new Zipper(zipFile);
                zipper.UnZipDirectory(packageDirectory);
                File.Delete(zipper.ZipLocation);
                var result = new AssemblySigner(packageDirectory).Sign();

                switch (result)
                {
                    case SigningPackageResult.NoSigningRequired:
                    {
                        Console.WriteLine("Signing for parcel {0} not required.", signingPackage.Distribution.ParcelId);
                        signingPackage.Distribution.Signed = true;
                        try
                        {
                            var distribution =
                                _client.PutAsync<Distribution, Distribution>(
                                           "Distribution",
                                           null,
                                           signingPackage.Distribution).Result;
                        }
                        catch (Exception)
                        {
                            throw new Exception("A communication error occurred.");
                        }
                        break;
                    }
                    case SigningPackageResult.DirectorySigned:
                    {
                        Console.WriteLine("Signing for parcel {0} completed.", signingPackage.Distribution.ParcelId);
                        signingPackage.Distribution.Signed = true;
                        zipper = new Zipper(zipFile);
                        zipper.ZipDirectory(packageDirectory);
                        signingPackage.ZipBytes = File.ReadAllBytes(zipFile);
                        var success =
                            _client.PutAsync<bool, SigningPackage>("SigningPackage", null, signingPackage).Result;
                        if (!success)
                        {
                            throw new Exception("A communication error occurred.");
                        }
                        break;
                    }
                    case SigningPackageResult.Error:
                    {
                        Console.WriteLine("There was an error signing parcel {0}", signingPackage.Distribution.ParcelId);
                        break;
                    }
                    default:
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }
    }
}