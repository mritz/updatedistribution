﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.ServiceProcess;
using System.Web.Http;
using System.Web.Http.SelfHost;

using Topshelf;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.BuildWatch
{
    public sealed partial class BuildWatchService : ServiceBase
    {
        private readonly HttpSelfHostConfiguration _serverConfiguration;

        private HttpSelfHostServer _server;

        public BuildWatchService()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var selfHostUrl = ConfigurationManager.AppSettings["SelfHostUrl"];
            Logger.Info(string.Format("Self-host URL: {0}", selfHostUrl));
            _serverConfiguration = new HttpSelfHostConfiguration(selfHostUrl);
            _serverConfiguration.MapHttpAttributeRoutes();
            _serverConfiguration.Routes.MapHttpRoute(
                                    "DefaultApi",
                                    "api/{controller}/{id}",
                                    new {id = RouteParameter.Optional});

            InitializeComponent();
        }

        public void ForceStart()
        {
            OnStart(null);
        }
        public void ForceStop()
        {
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            Logger.Info("Starting service");
            NetworkHelper.OpenPort(2345);
            _server = new HttpSelfHostServer(_serverConfiguration);
            _server.OpenAsync();
        }
        protected override void OnStop()
        {
            Logger.Info("Stopping service");
            _server.CloseAsync().Wait();
            _server.Dispose();
            NetworkHelper.ClosePort(2345);

        }

        [LoaderOptimization(LoaderOptimization.MultiDomainHost)]
        [STAThread]
        private static void Main(string[] args)
        {
            try
            {
                HostFactory.Run(
                    x =>
                    {
                        x.StartAutomaticallyDelayed();
                        x.Service<BuildWatchService>(
                            s =>
                            {
                                s.ConstructUsing(name => new BuildWatchService());
                                s.WhenStarted(tc => tc.ForceStart());
                                s.WhenStopped(tc => tc.ForceStop());
                            });
                        x.RunAsLocalSystem();
                        x.SetDescription("Build listener for dev update distribution");
                        x.SetDisplayName("Update Distributor Build Listener");
                        x.SetServiceName("UpdateDistributorBuildListener");

                        x.EnableServiceRecovery(
                            r =>
                            {
                                r.RestartService(1);
                                r.RestartService(2);
                                r.SetResetPeriod(1);
                            });
                    });
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
        }
    }
}