﻿using System.Threading.Tasks;
using System.Web.Http;

using UpdateDistributor.BuildWatch.Deployment;
using UpdateDistributor.BuildWatch.Vsts;
using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.BuildWatch.Controllers
{
    [AdminAuthorize]
    public sealed class BuildListenerController : ApiController
    {
        [Route("api/BuildListener/BuildCompleted")]
        [HttpPost]
        public bool BuildCompleted([FromBody] VstsBuild build)
        {
            new TaskFactory().StartNew(
                                 () =>
                                 {
                                     Logger.Info(
                                         string.Format(
                                             "Build triggered: \r\nName: {0}\r\nBuild #: {1}\r\nFinish time: {2}\r\nStatus: {3}",
                                             build.Definition.Name,
                                             build.BuildNumber,
                                             build.FinishTime,
                                             build.Status));

                                     if (build.Definition.Name.Contains("TaxSlayer"))
                                     {
                                         var taxYear = build.BuildNumber.Substring(9);

                                         if (build.Status == BuildStatus.Succeeded)
                                         {
                                             new BuildDeployer().DeployBuildToDev(taxYear);
                                         }
                                     }
                                 });
            return true;
        }
        [Route("api/BuildListener/TriggerBuild/{taxYear}")]
        [HttpPost]
        public bool TriggerBuild(string taxYear)
        {
            new TaskFactory().StartNew(
                                 () =>
                                 {
                                     Logger.Info(string.Format("Build triggered for: {0}", taxYear));
                                     new BuildDeployer().DeployBuildToDev(taxYear);
                                 });
            return true;
        }
    }
}