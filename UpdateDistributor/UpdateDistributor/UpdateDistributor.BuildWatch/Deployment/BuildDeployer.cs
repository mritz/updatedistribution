﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using Database.Core.TaxReturnCore;

using RCS.Common.Compression;
using RCS.Common.Extensions;
using RCS.Common.FoxPro.Strings;
using RCS.Common.Serialization;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Client.Enumerations;
using UpdateDistributor.Core.Extensions;
using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Nuget;
using UpdateDistributor.Core.Rest;
using UpdateDistributor.Core.Utilities;

namespace UpdateDistributor.BuildWatch.Deployment
{
    public sealed class BuildDeployer
    {
        private readonly IDeploymentHelper _deploymentHelper;
        private readonly object _syncObject = new object();

        public BuildDeployer()
        {
            var restApiUrl = ConfigurationManager.AppSettings["UpdateDistributorRestApi"];
            _deploymentHelper = new DeploymentHelper(new RestClient(restApiUrl));
            Logger.Info(string.Format("Build deployer initialized with Rest API: {0}", restApiUrl));
        }

        public void DeployBuildToDev(string taxYear)
        {
            lock (_syncObject)
            {
                Logger.Info(string.Format("Deploying {0} build to dev Nuget server.", taxYear));
                var segments = _deploymentHelper.GetSegments();

                if (segments == null)
                {
                    Logger.Warn("Segments object was null.");
                    return;
                }

                foreach (var segment in segments)
                {
                    var parcels = _deploymentHelper.GetParcelsBySegmentId(segment.Id, taxYear);

                    foreach (var parcel in parcels)
                    {
                        try
                        {
                            var oldDistribution =
                                _deploymentHelper.GetActiveDistributionForEnvironment(
                                    parcel.Id,
                                    (int)ReleaseLevel.Development) ??
                                new Distribution
                                {
                                    Id = -1,
                                    ParcelId = parcel.Id,
                                    Environment = (int)ReleaseLevel.Development
                                };

                            var newDistribution = new Distribution
                            {
                                Id = -1,
                                ParcelId = parcel.Id,
                                Environment = (int)ReleaseLevel.Development,
                                Release = DateTime.Now,
                                Version = GetFileVersion(parcel, oldDistribution),
                                Active = true
                            };

                            var success = PushNugetPackage(
                                taxYear,
                                parcel,
                                newDistribution,
                                _deploymentHelper);

                            if (success)
                            {
                                _deploymentHelper.UpdateDistribution(newDistribution);
                                UpdateSupportedEnvironments(parcel.Id, parcel.TaxYear);
                            }
                            else
                            {
                                throw new Exception("Unable to distribute Nuget package.");
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e.Message, e);
                        }
                    }
                }
                Logger.Info("Deployment complete.");
            }
        }

        private static string GetAssemblyVersion(string filePath)
        {
            var fvi = FileVersionInfo.GetVersionInfo(filePath);
            return fvi.FileVersion;
        }
        private static string GetFlatVersion(Parcel parcel, Distribution distribution)
        {
            var version = distribution.Version;

            if (version.IsNullOrWhiteSpace() || (version.Count(c => c == '.') != 3))
            {
                version = string.Format(
                    "{0}.1.{1}.{2}",
                    (parcel.TaxYear == "Dev" ? ConfigurationManager.AppSettings["TaxYear"] : parcel.TaxYear).Substring(
                        2,
                        2),
                    DateTime.Now.ToJulianDate(),
                    version);
            }

            var versionSplit = version.Split('.');

            var dailyBuildCount = versionSplit[3].ToInt32(0);
            dailyBuildCount = versionSplit[2] == DateTime.Now.ToJulianDate().ToString() ? dailyBuildCount + 1 : 1;

            return string.Format(
                "{0}.{1}.{2}.{3}",
                (parcel.TaxYear == "Dev" ? ConfigurationManager.AppSettings["TaxYear"] : parcel.TaxYear).Substring(2, 2),
                1,
                DateTime.Now.ToJulianDate(),
                dailyBuildCount);
        }

        private string GetFileVersion(Parcel parcel, Distribution distribution)
        {
            var keyFileBundle =
                _deploymentHelper.GetFileBundlesByParcelId(parcel.Id)
                                 .ToList()
                                 .FirstOrDefault(
                                     x => Path.GetFileName(x.BuildPath) == Path.GetFileName(parcel.KeyFilePath));

            return (keyFileBundle != null) && keyFileBundle.BuildPath.EndsWith(".dll")
                       ? GetAssemblyVersion(GetFormattedBuildPath(keyFileBundle, parcel.TaxYear))
                       : GetFlatVersion(parcel, distribution);
        }
        private void UpdateSupportedEnvironments(int parcelId, string taxYear)
        {
            var supportedEnvironments = _deploymentHelper.GetSupportedForParcel(parcelId).ToList();
            var stateAssemblyFileBundle =
                _deploymentHelper.GetFileBundlesByParcelId(parcelId)
                                 .FirstOrDefault(x => Regex.IsMatch(x.FileName, @"States\.[A-Z]{2}\.dll"));

            if (stateAssemblyFileBundle == null)
                return;

            var stateAssemblyFile = stateAssemblyFileBundle.BuildPath;
            var stateAbbreviation =
                Path.GetFileName(stateAssemblyFile.Replace("States.", string.Empty).Replace(".dll", string.Empty));

            Assembly assembly;
            try
            {
                assembly = Assembly.LoadFrom(GetFormattedBuildPath(stateAssemblyFileBundle, taxYear));
            }
            catch (Exception e)
            {
                Logger.Error("Could not load assembly from file.", e);
                foreach (var supported in supportedEnvironments)
                {
                    supported.Entities = string.Empty;
                    _deploymentHelper.UpdateSupported(supported);
                }
                return;
            }

            foreach (ReleaseLevel releaseLevel in Enum.GetValues(typeof(ReleaseLevel)))
            {
                var supported = supportedEnvironments.FirstOrDefault(x => x.Environment == (int)releaseLevel) ??
                                new SupportedEnvironment {ParcelId = parcelId, Environment = (int)releaseLevel};

                try
                {
                    var entities = new StringBuilder();
                    var exportedTypes = assembly.GetTypes().ToList();
                    foreach (var taxEntity in from taxEntity in Enum.GetValues(typeof(TaxEntity)).Cast<TaxEntity>()
                                              let module =
                                              exportedTypes.FirstOrDefault(
                                                  x =>
                                                      x.Name ==
                                                      string.Format(
                                                          "{0}{1}Module",
                                                          stateAbbreviation,
                                                          taxEntity == TaxEntity.Individual
                                                              ? string.Empty
                                                              : taxEntity.ToString()))
                                              where module != null
                                              let releaseInfo = module.GetCustomAttribute<ReleaseInfoAttribute>()
                                              where releaseInfo != null
                                              where releaseInfo.ReleaseLevel >= releaseLevel
                                              select taxEntity)
                        entities.AppendFormat("{0}|", taxEntity);

                    supported.Entities = entities.ToString().Trim('|');
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message, e);
                    supported.Entities = string.Empty;
                }
                finally
                {
                    _deploymentHelper.UpdateSupported(supported);
                }
            }
        }
        private static string GetFormattedBuildPath(FileBundle fileBundle, string taxYear)
        {
            var buildRoot = Path.GetExtension(fileBundle.FileName).InList(".pd_", ".xslt")
                                ? string.Format(
                                    "{0} Forms Xslt",
                                    taxYear == "Dev" ? ConfigurationManager.AppSettings["TaxYear"] : taxYear)
                                : string.Format("TaxSlayer{0}", taxYear);
            return string.Format(fileBundle.BuildPath, buildRoot);
        }

        private static void AddFileBundleToArchive(FileBundle fileBundle, Zipper zipper, string taxYear)
        {
            var formattedBuildPath = GetFormattedBuildPath(fileBundle, taxYear);
            if (fileBundle.BuildPath.Contains("*"))
            {
                var directoryName = Path.GetDirectoryName(formattedBuildPath);
                if (Directory.Exists(directoryName))
                {
                    var files = Directory.GetFiles(directoryName, Path.GetFileName(formattedBuildPath));
                    foreach (var file in files)
                    {
                        Logger.Info(string.Format("Adding {0} to archive.", file));
                        try
                        {
                            var bytes = File.ReadAllBytes(file);
                            zipper.AppendToZipFile(Path.GetFileName(file), bytes);
                        }
                        catch (Exception e)
                        {
                            Logger.Error(string.Format("Unable to add {0} to archive.", file), e);
                        }
                    }
                }
                else
                {
                    Logger.Warn(string.Format("Can't find directory: {0}", directoryName));
                }
            }
            else
            {
                Logger.Info(string.Format("Adding {0} to archive.", formattedBuildPath));
                try
                {
                    var bytes = File.ReadAllBytes(formattedBuildPath);
                    zipper.AppendToZipFile(Path.GetFileName(fileBundle.FilePath), bytes);
                }
                catch (Exception e)
                {
                    Logger.Error(string.Format("Unable to add {0} to archive.", fileBundle.FilePath), e);
                }
            }
        }

        private static bool PushNugetPackage(
    string taxYear, Parcel parcel, Distribution distribution, IDeploymentHelper deploymentHelper)
        {
            var fileBundles = deploymentHelper.GetFileBundlesByParcelId(parcel.Id).ToList();
            var zipper = new Zipper(Path.Combine(Path.GetTempPath(), "nuget.zip"));
            //zipper.Password = "KJ0093J8ALKJFDJAJ20";

            NugetHelper.FlushZipArchive(zipper);

            var manifest = new List<ManifestEntry>();
            foreach (var fileBundle in fileBundles)
            {
                AddFileBundleToArchive(fileBundle, zipper, taxYear);
                if (fileBundle.FileName.Contains("*"))
                {
                    var formattedBuildPath = GetFormattedBuildPath(fileBundle, taxYear);
                    var directory = Path.GetDirectoryName(formattedBuildPath);
                    if (directory != null)
                    {
                        var files = Directory.GetFiles(directory, Path.GetFileName(formattedBuildPath));
                        manifest.AddRange(
                            files.Select(
                                file =>
                                {
                                    var path = Path.GetDirectoryName(fileBundle.FilePath);
                                    var filePath = Path.GetFileNameWithoutExtension(file) +
                                                   Path.GetExtension(fileBundle.FilePath);
                                    if (path != null)
                                        filePath = Path.Combine(path, filePath);

                                    return new ManifestEntry
                                    {
                                        Id = fileBundle.Id,
                                        ParcelId = fileBundle.ParcelId,
                                        FileName = Path.GetFileName(file),
                                        FilePath = filePath,
                                        FileType = (UpdateFileType)fileBundle.FileType,
                                        VersionCheck = distribution.Version
                                    };
                                }));
                    }
                }
                else
                {
                    manifest.Add(
                        new ManifestEntry
                        {
                            Id = fileBundle.Id,
                            ParcelId = fileBundle.ParcelId,
                            FileName = fileBundle.FileName,
                            FilePath = fileBundle.FilePath,
                            FileType = (UpdateFileType)fileBundle.FileType,
                            VersionCheck = distribution.Version
                        });
                }
            }

            var directoryName = Path.GetDirectoryName(zipper.ZipLocation);
            if (directoryName != null)
            {
                const string manifestFileName = "Manifest.xml";
                var manifestLocation = Path.Combine(directoryName, manifestFileName);
                if (File.Exists(manifestLocation))
                    File.Delete(manifestLocation);
                ObjectXmlSerializer<List<ManifestEntry>>.Save(manifestLocation, manifest);
                using (var stream = File.Open(manifestLocation, FileMode.Open))
                {
                    zipper.AppendToZipFile(manifestFileName, stream);
                }
                File.Delete(manifestLocation);
            }

            var zipBytes = File.ReadAllBytes(zipper.ZipLocation);
            var nugetDistribution = new NugetDistribution(taxYear, parcel, distribution, zipBytes);
            var success = deploymentHelper.DistributeNugetPackage(nugetDistribution);

            NugetHelper.FlushZipArchive(zipper);
            return success;
        }

    }
}