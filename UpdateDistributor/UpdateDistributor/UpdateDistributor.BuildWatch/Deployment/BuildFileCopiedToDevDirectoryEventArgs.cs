﻿namespace UpdateDistributor.BuildWatch.Deployment
{
    public class BuildFileCopiedToDevDirectoryEventArgs
    {
        public BuildFileCopiedToDevDirectoryEventArgs(object userState, int progressPercentage, int taxYear)
        {
            UserState = userState;
            ProgressPercentage = progressPercentage;
            TaxYear = taxYear;
        }

        public int ProgressPercentage { get; set; }
        public int TaxYear { get; set; }
        public object UserState { get; set; }
    }
}