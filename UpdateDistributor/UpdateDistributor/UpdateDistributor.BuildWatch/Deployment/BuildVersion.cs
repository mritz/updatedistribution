﻿using System;

namespace UpdateDistributor.BuildWatch.Deployment
{
    public class BuildVersion
    {
        public BuildVersion(int taxYear)
        {
            TaxYear = taxYear;
        }
        public BuildVersion(int taxYear, int revision, DateTime date)
        {
            TaxYear = taxYear;
            Revision = revision;
            Date = date;
        }

        public DateTime Date { get; set; }
        public int Revision { get; set; }
        public int TaxYear { get; set; }
    }
}