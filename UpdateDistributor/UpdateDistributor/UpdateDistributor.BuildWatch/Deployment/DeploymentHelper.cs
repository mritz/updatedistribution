﻿using System;
using System.Collections.Generic;
using System.Linq;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Nuget;
using UpdateDistributor.Core.Rest;
using UpdateDistributor.Core.Utilities;

namespace UpdateDistributor.BuildWatch.Deployment
{
    public sealed class DeploymentHelper : IDeploymentHelper
    {
        private readonly RestClient _client;

        public DeploymentHelper(RestClient client)
        {
            _client = client;
        }

        #region IDeploymentHelper Members

        public bool DistributeNugetPackage(NugetDistribution nugetDistribution)
        {
            Logger.Info(
                string.Format(
                    "Pushing package to Nuget server:\r\nName: {0}\r\nVersion: {1}\r\nFile Size: {2}",
                    nugetDistribution.Parcel.FileNameOfParcel,
                    nugetDistribution.Distribution.Version,
                    nugetDistribution.ZipBytes.Length));
            var success =
                _client.PostAsync<bool, NugetDistribution>("Nuget", "DistributeNugetPackage", nugetDistribution).Result;
            Logger.Info(success ? "Successful" : "Failed");
            return success;
        }
        public Distribution GetActiveDistributionForEnvironment(int parcelId, int environment)
        {
            Logger.Info(
                string.Format(
                    "Retrieving active {1} distribution for parcel ID: {0}",
                    parcelId,
                    (ReleaseLevel)environment));
            var distribution = _client.GetAsync<Distribution>("Distribution", null, parcelId, environment).Result;
            if (distribution != null)
            {
                Logger.Info(string.Format("Found distribution with ID: {0}", distribution.Id));
            }
            return distribution;

        }
        public IEnumerable<FileBundle> GetFileBundlesByParcelId(int parcelId)
        {
            Logger.Info(string.Format("Retrieving file bundles for parcel ID: {0}", parcelId));
            var fileBundles =
                _client.GetAsync<IEnumerable<FileBundle>>("FileBundle", "GetByParcelId", parcelId).Result.ToList();
            Logger.Info(string.Format("Found {0} file bundle(s)", fileBundles.Count));
            return fileBundles;
        }
        public Parcel GetParcelById(int id)
        {
            Logger.Info(string.Format("Retrieving parcel for ID: {0}", id));
            var parcel = _client.GetAsync<Parcel>("Parcel", "Get", id).Result;
            if (parcel != null)
            {
                Logger.Info(string.Format("Found parcel with ID: {0}", parcel.Id));
            }
            return parcel;
        }
        public IEnumerable<Parcel> GetParcelsBySegmentId(int segmentId, string taxYear)
        {
            Logger.Info(string.Format("Retrieving parcels for segment ID: {0}", segmentId));
            var parcels =
                _client.GetAsync<IEnumerable<Parcel>>("Parcel", "GetBySegmentId", segmentId, taxYear).Result.ToList();
            Logger.Info(string.Format("Found {0} parcel(s)", parcels.Count));
            return parcels;
        }
        public IEnumerable<Segment> GetSegments()
        {
            Logger.Info("Retrieving segments");
            var segments = _client.GetAsync<IEnumerable<Segment>>("Segment", null).Result.ToList();
            Logger.Info(string.Format("Found {0} segment(s)", segments.Count));
            return segments;
        }
        public IEnumerable<SupportedEnvironment> GetSupportedForParcel(int parcelId)
        {
            Logger.Info("Retrieving supported environments for parcel");
            var supportedEnvironments =
                _client.GetAsync<IEnumerable<SupportedEnvironment>>("SupportedEnvironment", null, parcelId)
                       .Result.ToList();
            Logger.Info(string.Format("Found {0} supported environment(s)", supportedEnvironments.Count));
            return supportedEnvironments;
        }
        public void UpdateDistribution(Distribution distribution)
        {
            distribution = distribution.Id == -1
                               ? _client.PostAsync<Distribution, Distribution>("Distribution", null, distribution)
                                        .Result
                               : _client.PutAsync<Distribution, Distribution>("Distribution", null, distribution).Result;
            if (distribution == null)
                throw new InvalidOperationException("Distribution table not updated.");
        }
        public void UpdateParcel(Parcel parcel)
        {
            parcel = _client.PutAsync<Parcel, Parcel>("Parcel", null, parcel).Result;
            if (parcel == null)
                throw new InvalidOperationException("Parcel table not updated.");
        }
        public void UpdateSupported(SupportedEnvironment supportedEnvironment)
        {
            supportedEnvironment = supportedEnvironment.Id == -1
                                       ? _client.PostAsync<SupportedEnvironment, SupportedEnvironment>(
                                                    "SupportedEnvironment",
                                                    null,
                                                    supportedEnvironment).Result
                                       : _client.PutAsync<SupportedEnvironment, SupportedEnvironment>(
                                                    "SupportedEnvironment",
                                                    null,
                                                    supportedEnvironment).Result;
            if (supportedEnvironment == null)
                throw new InvalidOperationException("Supported table not updated");
        }

        #endregion
    }
}