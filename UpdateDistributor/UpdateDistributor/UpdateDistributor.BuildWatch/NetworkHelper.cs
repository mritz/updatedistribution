﻿using System;
using System.Security.Principal;
using NetFwTypeLib;
using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.BuildWatch
{
    public static class NetworkHelper
    {
        public static void OpenPort(int port)
        {
            try
            {
                if (CheckPermissions())
                {
                    OpenPortInternal(port);
                }
                else
                {
                    Logger.Info(string.Format("Unable to open port: {0}", port));
                }
            }
            catch (Exception ex)
            {
                Logger.Info(string.Format("Unable to open port: {0}. Error: {1}", port, ex));
            }

        }
        public static void ClosePort(int port)
        {
            try
            {
                if (CheckPermissions())
                {
                    ClosePortInternal(port);
                }
                else
                {
                    Logger.Info(string.Format("Unable to close port: {0}", port));
                }
            }
            catch (Exception ex)
            {
                Logger.Info(string.Format("Unable to close port: {0}. Error: {1}", port, ex));
            }

        }


        //Attempt to open the port
        private static void OpenPortInternal(int port)
        {
            try
            {
                var ticfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                var icfMgr = (INetFwMgr)Activator.CreateInstance(ticfMgr);
                var profile = icfMgr.LocalPolicy.CurrentProfile;
                var tportClass = Type.GetTypeFromProgID("HNetCfg.FWOpenPort");
                var portClass = (INetFwOpenPort)Activator.CreateInstance(tportClass);
                portClass.Scope = NET_FW_SCOPE_.NET_FW_SCOPE_ALL;
                portClass.Enabled = true;
                portClass.Name = "TaxSlayerDataService";
                portClass.Port = port;
                portClass.Protocol = NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;


                profile.GloballyOpenPorts.Add(portClass);

            }
            catch (Exception ex)
            {
                Logger.Error("Error",ex);
            }
        }

        //Attempt to close the port
        private static void ClosePortInternal(int port)
        {
            try
            {
                var ticfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                var icfMgr = (INetFwMgr)Activator.CreateInstance(ticfMgr);
                var profile = icfMgr.LocalPolicy.CurrentProfile;
                profile.GloballyOpenPorts.Remove(port, NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP);
            }
            catch (Exception ex)
            {
                Logger.Error("Error",ex);
            }
        }


        private static bool CheckPermissions()
        {
            var id = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(id);
            if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                return false;
            return Environment.OSVersion.Version >= new Version(6, 0);
        }
    }
}