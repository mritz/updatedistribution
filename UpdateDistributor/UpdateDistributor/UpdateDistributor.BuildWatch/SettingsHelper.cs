﻿using System;
using System.Collections.Generic;

using RCS.Common.Extensions;

using UpdateDistributor.BuildWatch.Deployment;
using UpdateDistributor.BuildWatch.Properties;

namespace UpdateDistributor.BuildWatch
{
    public static class SettingsHelper
    {
        public static IEnumerable<BuildVersion> GetLatestBuilds()
        {
            var builds = Settings.Default.LatestBuilds.Split(new[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var build in builds)
            {
                var components = build.Split(';');
                var taxYear = components[0].ToInt32(0);
                var revision = components[1].ToInt32(0);
                var date = components[2].ToDateTime() ?? DateTime.MinValue;
                yield return new BuildVersion(taxYear, revision, date);
            }
        }
    }
}