﻿using System;
using System.Collections.Generic;
using System.Linq;

using UpdateDistributor.BuildWatch.Properties;

namespace UpdateDistributor.BuildWatch.Tfs
{
    //public static class TfsBuildHelper
    //{
    //    public static IBuildDefinition[] GetAllBuildDefinitionsFromTheTeamProject(TfsConnection project)
    //    {
    //        var bs = project.GetService<IBuildServer>();
    //        return bs.QueryBuildDefinitions(Resources.CollectionName);
    //    }
    //    public static IList<KeyValuePair<string, Uri>> GetBuildDefinitionListFromProject(
    //        Guid collectionId, string projectName)
    //    {
    //        List<IBuildDefinition> buildDefinitionList = null;
    //        List<KeyValuePair<string, Uri>> buildDefinitionInfoList = null;

    //        try
    //        {
    //            buildDefinitionInfoList = new List<KeyValuePair<string, Uri>>();
    //            var tfsUri = new Uri(Resources.TfsAddress);
    //            var configurationServer = TfsConfigurationServerFactory.GetConfigurationServer(tfsUri);

    //            var tfsProjectCollection = configurationServer.GetTeamProjectCollection(collectionId);
    //            tfsProjectCollection.Authenticate();
    //            var buildServer = (IBuildServer)tfsProjectCollection.GetService(typeof(IBuildServer));
    //            buildDefinitionList = new List<IBuildDefinition>(buildServer.QueryBuildDefinitions(projectName));
    //        }
    //        catch (Exception e)
    //        {
    //            //ApplicationLogger.Log(e);
    //        }

    //        if ((buildDefinitionList != null) && (buildDefinitionList.Count > 0))
    //        {
    //            foreach (var builddef in buildDefinitionList)
    //            {
    //                buildDefinitionInfoList.Add(new KeyValuePair<string, Uri>(builddef.Name, builddef.Uri));
    //            }
    //        }
    //        return buildDefinitionInfoList;
    //    }
    //    public static BuildStatus GetBuildResult(string buildName)
    //    {
    //        var result = BuildStatus.None;
    //        var teamProjectCollection = GetTeamProjectCollection();
    //        if (teamProjectCollection != null)
    //        {
    //            result = GetLastBuildResult(teamProjectCollection, buildName);
    //        }
    //        return result;
    //    }
    //    public static IBuildDetail GetLastBuild(string buildName)
    //    {
    //        var teamProjectCollection = GetTeamProjectCollection();
    //        return teamProjectCollection != null ? GetLastBuildInternal(teamProjectCollection, buildName) : null;
    //    }
    //    public static IEnumerable<IQueuedBuild> GetQueuedBuilds(
    //        TfsTeamProjectCollection tfsCollection, string teamProject)
    //    {
    //        // Get instance of build server service from TFS
    //        var buildServer = tfsCollection.GetService<IBuildServer>();

    //        // Set up query
    //        var spec = buildServer.CreateBuildQueueSpec(teamProject);
    //        spec.Status = QueueStatus.All;

    //        // Execute query
    //        var result = buildServer.QueryQueuedBuilds(spec);

    //        // Array of queued builds will be in the result.QueuedBuilds property
    //        return result.QueuedBuilds;
    //    }
    //    public static TfsTeamProjectCollection GetTeamProjectCollection()
    //    {
    //        var tfsUri = new Uri(Resources.TfsAddress);

    //        var configurationServer = TfsConfigurationServerFactory.GetConfigurationServer(tfsUri);

    //        var collectionNodes =
    //            configurationServer.CatalogNode.QueryChildren(
    //                                   new[] {CatalogResourceTypes.ProjectCollection},
    //                                   false,
    //                                   CatalogQueryOptions.None);

    //        var collectionNode =
    //            collectionNodes.FirstOrDefault(
    //                x =>
    //                    string.Compare(
    //                        x.Resource.DisplayName,
    //                        Resources.TfsCollection,
    //                        StringComparison.OrdinalIgnoreCase) == 0);

    //        if (collectionNode == null)
    //            return null;

    //        var collectionId = new Guid(collectionNode.Resource.Properties["InstanceId"]);
    //        return configurationServer.GetTeamProjectCollection(collectionId);
    //    }
    //    public static void TestTfs()
    //    {
    //        var tfsUri = new Uri(Resources.TfsAddress);

    //        var configurationServer = TfsConfigurationServerFactory.GetConfigurationServer(tfsUri);

    //        var collectionNodes =
    //            configurationServer.CatalogNode.QueryChildren(
    //                                   new[] {CatalogResourceTypes.ProjectCollection},
    //                                   false,
    //                                   CatalogQueryOptions.None);

    //        // List the team project collections
    //        foreach (var collectionNode in collectionNodes)
    //        {
    //            // Use the InstanceId property to get the team project collection
    //            var collectionId = new Guid(collectionNode.Resource.Properties["InstanceId"]);
    //            var teamProjectCollection = configurationServer.GetTeamProjectCollection(collectionId);

    //            // Print the name of the team project collection
    //            Console.WriteLine("Collection: " + teamProjectCollection.Name);

    //            // Get a catalog of team projects for the collection
    //            var projectNodes = collectionNode.QueryChildren(
    //                new[] {CatalogResourceTypes.TeamProject},
    //                false,
    //                CatalogQueryOptions.None);

    //            var cb = GetCompletedBuilds(teamProjectCollection, Resources.CollectionName);

    //            var qb = GetQueuedBuilds(teamProjectCollection, Resources.CollectionName);

    //            foreach (var projectNode in projectNodes)
    //            {
    //                Console.WriteLine(" Team Project: " + projectNode.Resource.DisplayName);
    //            }
    //        }
    //    }

    //    private static IEnumerable<IQueuedBuild> GetCompletedBuilds(
    //        TfsTeamProjectCollection teamProjectCollection, string teamProject)
    //    {
    //        throw new NotImplementedException();
    //    }
    //    private static IBuildDetail GetLastBuildInternal(TfsConnection project, string buildName)
    //    {
    //        var buildServer = project.GetService<IBuildServer>();

    //        var def = buildServer.CreateBuildDetailSpec(Resources.CollectionName);
    //        def.MaxBuildsPerDefinition = 1;
    //        def.QueryOrder = BuildQueryOrder.FinishTimeDescending;
    //        def.DefinitionSpec.Name = buildName;
    //        def.Status = BuildStatus.All;

    //        var builds = buildServer.QueryBuilds(def).Builds;

    //        return builds.Any() ? builds.First() : null;
    //    }
    //    private static BuildStatus GetLastBuildResult(TfsConnection project, string buildName)
    //    {
    //        var lastBuild = GetLastBuildInternal(project, buildName);
    //        return lastBuild != null ? lastBuild.Status : BuildStatus.None;
    //    }
    //}
}