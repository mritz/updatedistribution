﻿namespace UpdateDistributor.BuildWatch.Vsts
{
    public enum BuildStatus
    {
        All,
        Failed,
        InProgress,
        None,
        NotStarted,
        PartiallySucceeded,
        Stopped,
        Succeeded
    }
}