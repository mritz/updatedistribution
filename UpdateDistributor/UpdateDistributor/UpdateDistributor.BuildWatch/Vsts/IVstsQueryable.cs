﻿namespace UpdateDistributor.BuildWatch.Vsts
{
    public class IVstsQueryable<T>
    {
        public string Count { get; set; }
        public T Value { get; set; }
    }
}