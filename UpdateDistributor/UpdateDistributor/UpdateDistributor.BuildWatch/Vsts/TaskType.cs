﻿using System.ComponentModel;

namespace UpdateDistributor.BuildWatch.Vsts
{
    public enum TaskType
    {
        [Description("builds")] Builds,
        [Description("definitions")] Definitions,
        [Description("qualities")] Qualities,
        [Description("queues")] Queues,
        [Description("requests")] Requests
    }
}