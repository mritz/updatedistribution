﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

using RCS.Common.Extensions;

namespace UpdateDistributor.BuildWatch.Vsts
{
    public abstract class VstsApiQuery
    {
        private const string PersonalAccessToken = "g3v5xbnu72zxzqekvm4mshwprv6ebrhoxadzmdj3ospjkuvehdza";
        private const string QueryUrl = "https://taxslayerllc-dev.visualstudio.com/{0}/_apis/build/{1}?api-version={2}";

        private readonly BuildType _buildType;
        private readonly Dictionary<string, string> _parameters;
        private readonly string _project;
        private readonly TaskType _taskType;

        protected VstsApiQuery(string project, TaskType taskType, BuildType buildType)
        {
            _parameters = new Dictionary<string, string>();
            _project = project;
            _taskType = taskType;
            _buildType = buildType;
        }
        
        protected static HttpClient GetHttpClient()
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", "", PersonalAccessToken))));

            return client;
        }

        protected void AddParameter(string parameter, string value)
        {
            if (_parameters.ContainsKey(parameter))
            {
                _parameters[parameter] = value;
            }
            else
            {
                _parameters.Add(parameter, value);
            }
        }
        protected void AddParameter(string parameter, int value)
        {
            AddParameter(parameter, value.ToString());
        }
        protected string GetRequestUri()
        {
            var requestUri =
                _parameters.Aggregate(
                    string.Format(QueryUrl, _project, _taskType.GetDescription(), (int)_buildType),
                    (current, parameter) => current + string.Format("&{0}={1}", parameter.Key, parameter.Value));
            return requestUri;
        }
    }
}