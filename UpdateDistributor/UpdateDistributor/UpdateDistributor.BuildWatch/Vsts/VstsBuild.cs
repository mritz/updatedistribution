﻿namespace UpdateDistributor.BuildWatch.Vsts
{
    public class VstsBuild
    {
        public string BuildNumber { get; set; }
        public VstsDefinition Definition { get; set; }
        public string FinishTime { get; set; }
        public string Id { get; set; }
        public string Reason { get; set; }
        public string StartTime { get; set; }
        public BuildStatus Status { get; set; }
        public string Uri { get; set; }
        public string Url { get; set; }
    }
}