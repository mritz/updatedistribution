﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace UpdateDistributor.BuildWatch.Vsts
{
    public class VstsBuildsQuery : VstsApiQuery
    {
        private VstsBuildsQuery(string project, BuildType buildType) : base(project, TaskType.Builds, buildType) {}

        public static VstsBuildsQuery Create(string project, BuildType buildType)
        {
            return new VstsBuildsQuery(project, buildType);
        }

        public async Task<IEnumerable<VstsBuild>> Execute()
        {
            try
            {
                using (var client = GetHttpClient())
                {
                    using (var response = client.GetAsync(GetRequestUri()).Result)
                    {
                        response.EnsureSuccessStatusCode();
                        return await response.Content.ReadAsStringAsync().ContinueWith(
                                                 task =>
                                                 {
                                                     var json = task.Result;
                                                     var deserializeObject =
                                                         JsonConvert
                                                             .DeserializeObject<IVstsQueryable<IEnumerable<VstsBuild>>>(
                                                                 json);

                                                     return deserializeObject.Value;
                                                 });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }
        public VstsBuildsQuery OfQuality(string quality)
        {
            AddParameter("quality", quality);
            return this;
        }
        public VstsBuildsQuery RequestedFor(string requestedFor)
        {
            AddParameter("requestedFor", requestedFor);
            return this;
        }
        public VstsBuildsQuery Skip(int skip)
        {
            AddParameter("$skip", skip);
            return this;
        }
        public VstsBuildsQuery Top(int top)
        {
            AddParameter("$top", top);
            return this;
        }
        public VstsBuildsQuery WithDefinition(string definition)
        {
            AddParameter("definition", definition);
            return this;
        }
        public VstsBuildsQuery WithMinFinishTime(DateTime minFinishTime)
        {
            AddParameter(
                "minFinishTime",
                string.Format("{0}-{1}-{2}", minFinishTime.Month, minFinishTime.Day, minFinishTime.Year));
            return this;
        }
        public VstsBuildsQuery WithStatus(params BuildStatus[] statuses)
        {
            AddParameter(
                "status",
                statuses.Select(status => status.ToString())
                        .Aggregate((s1, s2) => string.Format("{0},{1}", s1.ToString(), s2.ToString())));
            return this;
        }
    }
}