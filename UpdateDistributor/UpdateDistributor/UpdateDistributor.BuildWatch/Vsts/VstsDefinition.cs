﻿namespace UpdateDistributor.BuildWatch.Vsts
{
    public class VstsDefinition
    {
        public string DefinitionType { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}