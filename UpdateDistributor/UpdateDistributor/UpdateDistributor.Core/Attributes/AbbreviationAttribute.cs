﻿using System;

namespace UpdateDistributor.Core.Attributes
{
    public class AbbreviationAttribute : Attribute
    {
        public AbbreviationAttribute(string abbreviation)
        {
            Abbreviation = abbreviation;
        }

        public string Abbreviation { get; set; }
    }
}