﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;

using Newtonsoft.Json;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Serialization;

namespace UpdateDistributor.Core.Communication
{
    /// <summary>
    ///     Class for Handling Web Post
    /// </summary>
    public static class Rest
    {
        /// <summary>
        ///     Sends HTTP Get Request to the specified URL and returns the result of the request.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="customHeaders">Dictionary of Custom HTTP Headers for the Request </param>
        /// <param name="httpVerb">Http Verb to Send </param>
        /// <param name="timeout">Request Timeout in milliseconds</param>
        /// <returns></returns>
        public static string HttpGet(
            string uri, Dictionary<string, string> customHeaders = null, string httpVerb = "GET", int timeout = 30000)
        {
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(uri);
                SetSecurity();
                req.Timeout = timeout;
                req.Method = httpVerb;
                req.UserAgent =
                    @"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36";
                AddCustomHeaders(customHeaders, req);
                var resp = req.GetResponse();
                var result = GetResult(resp);
                return result;
            }
            catch (WebException e)
            {
                try
                {
                    throw RetrieveServerError(e);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.ToString(), ex);
                    throw e;
                }
            }
        }
        /// <summary>
        ///     Posts Parameters to the specified URL and returns the result of the request.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="parameters">The parameters to post.  Format should be name=value</param>
        /// <param name="contentType">Content Type of Request - (default "application/x-www-form-urlencoded")</param>
        /// <param name="customHeaders">Dictionary of Custom HTTP Headers for the Request </param>
        /// <param name="httpVerb">Http Verb to Send </param>
        /// <param name="timeout">Request Timeout in milliseconds</param>
        /// <param name="isEncrypted">If true the parameters are encrypted and encryption headers are added.</param>
        /// <returns>http result</returns>
        public static string HttpPost(
            string uri, string parameters, string contentType = "application/x-www-form-urlencoded",
            Dictionary<string, string> customHeaders = null, string httpVerb = "POST", int timeout = 30000,
            bool isEncrypted = false)
        {
            try
            {
                ServicePointManager.DefaultConnectionLimit = 1000;
                var req = (HttpWebRequest)WebRequest.Create(uri);
                SetSecurity();
                //req.Proxy = new System.Net.WebProxy(ProxyString, true);
                req.Timeout = timeout;
                req.ContentType = contentType;
                req.Method = httpVerb;
                req.KeepAlive = false;
                req.ProtocolVersion = HttpVersion.Version10;
                req.ServicePoint.Expect100Continue = false;
                AddCustomHeaders(customHeaders, req);
                SetContent(parameters, req, isEncrypted);
                var resp = req.GetResponse();
                var result = GetResult(resp);
                return result;
            }
            catch (WebException e)
            {
                throw RetrieveServerError(e);
            }
        }

        /// <summary>
        ///     Add headers to web request
        /// </summary>
        /// <param name="customHeaders">Dictionary of Custom HTTP Headers for the Request </param>
        /// <param name="req">request to add to</param>
        private static void AddCustomHeaders(Dictionary<string, string> customHeaders, WebRequest req)
        {
            if (customHeaders == null)
                return;
            foreach (var customHeader in customHeaders)
            {
                req.Headers.Add(customHeader.Key, Regex.Replace(customHeader.Value, @"[\u0000-\u001F]", string.Empty));
            }
        }
        private static string GetResult(WebResponse resp)
        {
            string result;
            var isEncrypted = false;
            var encryptedHashCheck = "";
            if ((resp.Headers != null) && resp.Headers.HasKeys() && resp.Headers.AllKeys.Contains("Encrypted"))
            {
                isEncrypted = true;
                encryptedHashCheck = resp.Headers["EncryptedHashCheck"];
            }

            using (var responseStream = resp.GetResponseStream())
            {
                var sr = new StreamReader(responseStream, Encoding.UTF8);
                result = sr.ReadToEnd().Trim();
                sr.Close();
            }
            resp.Close();
            if (isEncrypted)
            {
                result = StringCipher.Decrypt(result, encryptedHashCheck);
            }
            return result;
        }
        private static Exception RetrieveServerError(WebException e)
        {
            try
            {
                if ((e != null) && (e.Response != null))
                {
                    var responseStream = e.Response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var jsonException = new StreamReader(responseStream).ReadToEnd();
                        var httpError = JsonConvert.DeserializeObject<HttpError>(jsonException);
                        var message = jsonException;
                        if (httpError.ContainsKey("ErrorCode"))
                        {
                            message = string.Format("Server Error Code: {0}", httpError["ErrorCode"]);
                        }
                        var serverError = new Exception(message, new Exception(httpError.Message));
                        return serverError;
                    }
                }
            }
            catch (Exception) {}
            return e ?? new Exception("Unknown server error occurred");
        }
        private static void SetContent(string parameters, WebRequest req, bool isEncrypted)
        {
            if (isEncrypted)
            {
                var key = Guid.NewGuid().ToString();
                var passKey = Hasher.HashIt(HashType.SHA1, key);
                parameters = StringCipher.Encrypt(parameters, passKey);
                req.Headers.Add("Encrypted", "True");
                req.Headers.Add("EncryptedKey", key);
                req.Headers.Add("EncryptedHashCheck", passKey);
            }

            var bytes = Encoding.ASCII.GetBytes(parameters);
            req.ContentLength = bytes.Length;
            using (var os = req.GetRequestStream())
            {
                os.Write(bytes, 0, bytes.Length);
            }
        }
        private static void SetSecurity()
        {
            if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Config", "irs.grp")))
            {
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                return;
            }
            var operatingSystem = Environment.OSVersion;
            if ((operatingSystem.Platform == PlatformID.Win32NT) && (operatingSystem.Version.Major == 6) &&
                (operatingSystem.Version.Minor == 0)) {}
            else
            {
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            }
        }
    }
}