﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace UpdateDistributor.Core.Encryption
{
    public static class FileHasher
    {
        #region HashType enum

        public enum HashType
        {
            MD5 = 0,
            SHA1 = 1
        }

        #endregion

        public static string ComputeFileHash(string fileName, HashType algorithToUse = HashType.MD5)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException(fileName);

            using (var file = new FileStream(fileName, FileMode.Open))
            {
                var fileBytes = GetBytes(algorithToUse, file);

                var sb = new StringBuilder();
                foreach (var fileByte in fileBytes)
                {
                    sb.Append(fileByte.ToString("x2"));
                }
                return sb.ToString();
            }
        }

        private static byte[] GetBytes(HashType algorithm, byte[] inBytes)
        {
            switch (algorithm)
            {
                case HashType.MD5:
                    using (var md5Hasher = MD5.Create())
                    {
                        return md5Hasher.ComputeHash(inBytes);
                    }
                case HashType.SHA1:
                    using (var sha1Hasher = SHA1.Create())
                    {
                        return sha1Hasher.ComputeHash(inBytes);
                    }

                default:
                    throw new ArgumentException("Invalid Algorithm Type");
            }
        }
        private static IEnumerable<byte> GetBytes(HashType algorithm, Stream inStream)
        {
            switch (algorithm)
            {
                case HashType.MD5:
                    using (var md5Er = MD5.Create())
                    {
                        return md5Er.ComputeHash(inStream);
                    }
                case HashType.SHA1:
                    using (var sha1Er = SHA1.Create())
                    {
                        return sha1Er.ComputeHash(inStream);
                    }
                default:
                    throw new ArgumentException("Invalid Algorithm Type");
            }
        }
    }
}