﻿using System;

using RCS.Common.Extensions;

namespace UpdateDistributor.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToDateDisplay(this DateTime dateTime)
        {
            if (dateTime == DateTime.MinValue)
                return "No Date";

            var monthName = string.Empty;
            switch (dateTime.Month)
            {
                case 1:
                    monthName = "January";
                    break;
                case 2:
                    monthName = "February";
                    break;
                case 3:
                    monthName = "March";
                    break;
                case 4:
                    monthName = "April";
                    break;
                case 5:
                    monthName = "May";
                    break;
                case 6:
                    monthName = "June";
                    break;
                case 7:
                    monthName = "July";
                    break;
                case 8:
                    monthName = "August";
                    break;
                case 9:
                    monthName = "September";
                    break;
                case 10:
                    monthName = "October";
                    break;
                case 11:
                    monthName = "November";
                    break;
                case 12:
                    monthName = "December";
                    break;
            }

            var daySuffix = "th";

            switch (dateTime.Day % 10)
            {
                case 1:
                    daySuffix = "st";
                    break;
                case 2:
                    daySuffix = "nd";
                    break;
                case 3:
                    daySuffix = "rd";
                    break;
            }

            return string.Format("{0} {1}{2} {3}", monthName, dateTime.Day, daySuffix, dateTime.Year);
        }
        public static double ToJulianDate(this DateTime dateTime)
        {
            var stringResult = string.Format("{0:yy}{1}", dateTime, dateTime.DayOfYear);
            return (double)stringResult.ToDecimal(0);
        }
    }
}