﻿using System;
using System.ComponentModel;

namespace UpdateDistributor.Core.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum enumType)
        {
            var type = enumType.GetType();
            var name = Enum.GetName(type, enumType);
            if (name != null)
            {
                var field = type.GetField(name);
                if (field != null)
                {
                    var descriptionAttribute =
                        Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (descriptionAttribute != null)
                        return descriptionAttribute.Description;
                }
            }
            return null;
        }
    }
}