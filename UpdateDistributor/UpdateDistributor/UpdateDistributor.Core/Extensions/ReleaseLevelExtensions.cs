﻿using System;

using TaxProgram.Infrastructure.Attributes.Modules;

namespace UpdateDistributor.Core.Extensions
{
    public static class ReleaseLevelExtensions
    {
        public static string GetPathForRelease(this ReleaseLevel releaseLevel)
        {
            switch (releaseLevel)
            {
                case ReleaseLevel.Development:
                    return "DEV";
                case ReleaseLevel.QualityAssurance:
                    return "QA";
                case ReleaseLevel.Production:
                    return "PROD";
                default:
                    throw new ArgumentOutOfRangeException("releaseLevel", releaseLevel, null);
            }
        }
    }
}