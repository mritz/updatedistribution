﻿using UpdateDistributor.Core.Attributes;
using UpdateDistributor.Core.Enumerations;

namespace UpdateDistributor.Core.Extensions
{
    public static class SegmentTypeExtensions
    {
        public static string GetAbbreviation(this SegmentType segmentType)
        {
            var type = segmentType.GetType();
            var memberInfos = type.GetMember(segmentType.ToString());
            if (memberInfos.Length > 0)
            {
                var attributes = memberInfos[0].GetCustomAttributes(typeof(AbbreviationAttribute), false);
                if (attributes.Length > 0)
                {
                    return ((AbbreviationAttribute)attributes[0]).Abbreviation;
                }
            }
            return string.Empty;
        }
    }
}