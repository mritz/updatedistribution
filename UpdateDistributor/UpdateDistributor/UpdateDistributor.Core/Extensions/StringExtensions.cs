﻿using System;

namespace UpdateDistributor.Core.Extensions
{
    public static class StringExtensions
    {
        public static DateTime ConvertYYYYMMDDToDateTime(this string value)
        {
            var year = value.Substring(0, 4).ToInt();
            var month = value.Substring(4, 2).ToInt();
            var day = value.Substring(6, 2).ToInt();

            try
            {
                return new DateTime(year, month, day);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }
        /// <summary>
        ///     Determines whether the specified expression is blank.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns>
        ///     <c>true</c> if the specified expression is blank; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsBlank(this string expression)
        {
            return string.IsNullOrWhiteSpace(expression);
        }
        public static int ToInt(this string value, int defaultNumber = 0)
        {
            int number;
            return int.TryParse(value, out number) ? number : defaultNumber;
        }
    }
}