﻿using System;
using System.Runtime.CompilerServices;

using log4net;

namespace UpdateDistributor.Core.Logging
{
    public class Logger
    {
        public static void Debug(
            object message, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Debug(message);
        }
        public static void Debug(
            object message, Exception exception, [CallerMemberName] string callerMemberName = "",
            [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Debug(message, exception);
        }
        public static void Error(
            object message, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Error(message);
        }
        public static void Error(
            object message, Exception exception, [CallerMemberName] string callerMemberName = "",
            [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Error(message, exception);
        }
        public static void Fatal(
            object message, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Fatal(message);
        }
        public static void Fatal(
            object message, Exception exception, [CallerMemberName] string callerMemberName = "",
            [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Fatal(message, exception);
        }
        public static void Info(
            object message, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Info(message);
        }
        public static void Info(
            object message, Exception exception, [CallerMemberName] string callerMemberName = "",
            [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Info(message, exception);
        }
        public static void Warn(
            object message, [CallerMemberName] string callerMemberName = "", [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Warn(message);
        }
        public static void Warn(
            object message, Exception exception, [CallerMemberName] string callerMemberName = "",
            [CallerLineNumber] int callerLineNumber = 0)
        {
            GetLogger(callerMemberName, callerLineNumber).Warn(message, exception);
        }

        private static ILog GetLogger(string callerMemberName, int callerLineNumber)
        {
            return LogManager.GetLogger(string.Format("{0}, line {1}", callerMemberName, callerLineNumber));
        }
    }
}