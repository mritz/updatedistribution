﻿using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

namespace UpdateDistributor.Core.Models
{
    public class SigningPackage
    {
        public SigningPackage(string taxYear, Distribution distribution, byte[] zipBytes)
        {
            TaxYear = taxYear;
            Distribution = distribution;
            ZipBytes = zipBytes;
        }

        public Distribution Distribution { get; set; }
        public string TaxYear { get; set; }
        public byte[] ZipBytes { get; set; }
    }
}