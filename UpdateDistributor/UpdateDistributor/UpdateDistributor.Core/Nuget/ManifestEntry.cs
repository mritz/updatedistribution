﻿using UpdateDistributor.Client.Enumerations;
using UpdateDistributor.Core.Enumerations;

namespace UpdateDistributor.Core.Nuget
{
    public class ManifestEntry
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public UpdateFileType FileType { get; set; }
        public int Id { get; set; }
        public int ParcelId { get; set; }
        public string VersionCheck { get; set; }
    }
}