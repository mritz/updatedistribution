﻿using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

namespace UpdateDistributor.Core.Nuget
{
    public class NugetDistribution
    {
        public NugetDistribution(string taxYear, Parcel parcel, Distribution distribution, byte[] zipBytes)
        {
            TaxYear = taxYear;
            Parcel = parcel;
            Distribution = distribution;
            ZipBytes = zipBytes;
        }

        public Distribution Distribution { get; set; }
        public Parcel Parcel { get; set; }
        public string TaxYear { get; set; }
        public byte[] ZipBytes { get; set; }
    }
}