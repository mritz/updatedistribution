﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;

using NuGet;

using RCS.Common.Compression;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.Core.Nuget
{
    public class NugetHelper
    {
        public static DirectoryInfo BuildStagingDirectory(string tempDirectory)
        {
            Logger.Info(string.Format("Building staging directory: {0}", tempDirectory));
            var stagingDirectory = Path.Combine(tempDirectory, "lib");
            var stagingDirectoryInfo = Directory.CreateDirectory(stagingDirectory);
            Logger.Info("Completed");
            return stagingDirectoryInfo;
        }
        public static void CleanStagingDirectory(string tempDirectory)
        {
            Logger.Info("Cleaning staging directory");
            try
            {
                if (Directory.Exists(tempDirectory))
                {
                    Directory.Delete(tempDirectory, true);
                }
            }
            catch (Exception)
            {
                Thread.Sleep(1000);
                Directory.Delete(tempDirectory, true);
            }
            Logger.Info("Completed");
        }
        public static string CreateArchive(byte[] zipBytes, DirectoryInfo stagingDirectory)
        {
            Logger.Info("Copying all bytes to archive...");
            var zipFile = Path.Combine(stagingDirectory.FullName, "nuget.zip");
            File.WriteAllBytes(zipFile, zipBytes);
            Logger.Info("Completed");
            return zipFile;
        }
        public static Manifest CreateNugetManifest(
            string fileNameOfParcel, string version, DirectoryInfo libDirectoryInfo)
        {
            Logger.Info("Creating Nuget manifest...");
            // Create .nuspec
            var nuspec = new Manifest
            {
                Metadata =
                    new ManifestMetadata
                    {
                        Description = fileNameOfParcel,
                        Id = Path.GetFileNameWithoutExtension(fileNameOfParcel),
                        Version = version
                    },
                Files =
                    libDirectoryInfo.EnumerateFiles()
                                    .Select(
                                        fileInfo =>
                                            new ManifestFile
                                            {
                                                Source = fileInfo.FullName,
                                                Target = string.Format(@"lib\{0}", fileInfo.Name)
                                            })
                                    .ToList()
            };
            Logger.Info("Completed");
            return nuspec;
        }
        public static string CreateNugetPackage(
            string fileNameOfParcel, Manifest manifest, DirectoryInfo stagingDirectory)
        {
            Logger.Info("Creating Nuget package");
            var builder = new PackageBuilder {Authors = {"TaxSlayer LLC"}, Description = fileNameOfParcel, Id = "1"};
            Logger.Info("Populating metadata");
            builder.Populate(manifest.Metadata);

            foreach (var nuspecFile in manifest.Files)
            {
                builder.Files.Add(
                           new PhysicalPackageFile {SourcePath = nuspecFile.Source, TargetPath = nuspecFile.Target});
                Logger.Info(string.Format("Added {0} to {1} in package", nuspecFile.Source, nuspecFile.Target));
            }

            // Create .nupkg
            var parentDirectory = Path.GetDirectoryName(stagingDirectory.FullName);
            var packageFileName = Path.Combine(parentDirectory, "package.nupkg");
            using (var stream = File.Open(packageFileName, FileMode.OpenOrCreate))
            {
                builder.Save(stream);
            }
            Logger.Info("Completed creating Nuget package");
            return packageFileName;
        }
        public static void FlushZipArchive(Zipper zipper)
        {
            if (File.Exists(zipper.ZipLocation))
                File.Delete(zipper.ZipLocation);
        }
        public static string GetApiKey(ReleaseLevel releaseLevel)
        {
            Logger.Info(string.Format("Retrieving Nuget server Api Key for {0}", releaseLevel));
            var apiKey = ConfigurationManager.AppSettings[string.Format("{0}_Nuget_ApiKey", releaseLevel)];
            Logger.Info(string.Format("Retrieved Api Key: {0}", apiKey));
            return apiKey;
        }
        public static string GetNugetServer(ReleaseLevel releaseLevel)
        {
            Logger.Info(string.Format("Retrieving Nuget server URL for {0}", releaseLevel));
            var nugetServerUrl = ConfigurationManager.AppSettings[string.Format("{0}_Nuget_Url", releaseLevel)];
            Logger.Info(string.Format("Retrieved URL: {0}", nugetServerUrl));
            return nugetServerUrl;
        }
        public static void PushPackageToNugetServer(
            string fileNameOfParcel, ReleaseLevel releaseLevel, DirectoryInfo stagingDirectory, string packageSource)
        {
            Logger.Info(string.Format("Pushing {0} package to {1} Nuget server", fileNameOfParcel, releaseLevel));
            // Push it real good
            Logger.Info("Creating local repository");
            var parentDirectory = Path.GetDirectoryName(stagingDirectory.FullName);
            var localRepo =
                PackageRepositoryFactory.Default.CreateRepository(
                                            Path.Combine(Environment.CurrentDirectory, parentDirectory));
            var package = localRepo.GetPackages().ToList().FirstOrDefault();
            var packageFile = new FileInfo(packageSource);
            var size = packageFile.Length;
            var ps = new PackageServer(GetNugetServer(releaseLevel), "userAgent");
            var apiKey = GetApiKey(releaseLevel);
            Logger.Info("Pushing package");
            ps.PushPackage(apiKey, package, size, 60000, false);
            Logger.Info(
                string.Format("Completed pushing {0} package to {1} Nuget server", fileNameOfParcel, releaseLevel));
        }
        public static void UnpackArchiveToStagingDirectory(DirectoryInfo libDirectoryInfo, string zipFile)
        {
            Logger.Info(string.Format("Unpacking archive to staging directory: {0}", libDirectoryInfo.FullName));
            var zipper = new Zipper(zipFile);
            zipper.UnZipDirectory(libDirectoryInfo);
            File.Delete(zipFile);
            Logger.Info("Completed");
        }
    }
}