﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;

using RCS.Common.Extensions;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.Core.Rest
{
    public sealed class RestClient
    {
        private readonly HttpClient _client;

        public RestClient(string baseAddress)
        {
            Logger.Info("Initializing RestClient...");
            _client = new HttpClient {BaseAddress = new Uri(baseAddress)};
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            foreach (var requestHeader in GetDefaultRequestHeaders())
            {
                _client.DefaultRequestHeaders.Add(requestHeader.Key, requestHeader.Value);
            }
            Logger.Info("Complete.");
        }

        public async Task<T> DeleteAsync<T>(string controller, string method, params object[] parameters)
        {
            var item = default(T);

            try
            {
                var requestUri = string.Format(
                    "{0}/api/{1}{2}",
                    _client.BaseAddress,
                    controller,
                    method.IsNullOrWhiteSpace() ? string.Empty : "/" + method);
                requestUri = parameters.Aggregate(
                    requestUri,
                    (current, parameter) => string.Format("{0}/{1}", current, parameter));
                Logger.Info(string.Format("Calling: {0}", requestUri));
                var response = await _client.DeleteAsync(requestUri);
                Logger.Info(string.Format("Response: {0} - {1}", (int)response.StatusCode, response.ReasonPhrase));
                if (response.IsSuccessStatusCode)
                {
                    item = await response.Content.ReadAsAsync<T>();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }

            return item;
        }
        public async Task<T> GetAsync<T>(string controller, string method, params object[] parameters) where T : class
        {
            T item = null;

            try
            {
                var requestUri = string.Format(
                    "{0}/api/{1}{2}",
                    _client.BaseAddress,
                    controller,
                    method.IsNullOrWhiteSpace() ? string.Empty : "/" + method);
                requestUri = parameters.Aggregate(
                    requestUri,
                    (current, parameter) => string.Format("{0}/{1}", current, parameter));
                Logger.Info(string.Format("Calling: {0}", requestUri));
                var response = await _client.GetAsync(requestUri);
                Logger.Info(string.Format("Response: {0} - {1}", (int)response.StatusCode, response.ReasonPhrase));
                if (response.IsSuccessStatusCode)
                {
                    item = await response.Content.ReadAsAsync<T>();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }

            return item;
        }
        public async Task<TReturn> PostAsync<TReturn, TSend>(string controller, string method, TSend entity)
        {
            var item = default(TReturn);

            try
            {
                var requestUri = string.Format(
                    "{0}/api/{1}{2}",
                    _client.BaseAddress,
                    controller,
                    method.IsNullOrWhiteSpace() ? string.Empty : "/" + method);
                Logger.Info(string.Format("Calling: {0}", requestUri));
                var response = await _client.PostAsJsonAsync(requestUri, entity);
                Logger.Info(string.Format("Response: {0} - {1}", (int)response.StatusCode, response.ReasonPhrase));
                if (response.IsSuccessStatusCode)
                {
                    item = await response.Content.ReadAsAsync<TReturn>();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }

            return item;
        }
        public async Task<TReturn> PutAsync<TReturn, TSend>(string controller, string method, TSend entity)
        {
            var item = default(TReturn);

            try
            {
                var requestUri = string.Format(
                    "{0}/api/{1}{2}",
                    _client.BaseAddress,
                    controller,
                    method.IsNullOrWhiteSpace() ? string.Empty : "/" + method);
                Logger.Info(string.Format("Calling: {0}", requestUri));
                var response = await _client.PutAsJsonAsync(requestUri, entity);
                Logger.Info(string.Format("Response: {0} - {1}", (int)response.StatusCode, response.ReasonPhrase));
                if (response.IsSuccessStatusCode)
                {
                    item = await response.Content.ReadAsAsync<TReturn>();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }

            return item;
        }

        private static Dictionary<string, string> GetDefaultRequestHeaders()
        {
            return new Dictionary<string, string> {{"X-UpdateDistAuthTok", "08F01E0F-140B-4FE4-B493-4A3F2715FC0B"}};
        }
    }
}