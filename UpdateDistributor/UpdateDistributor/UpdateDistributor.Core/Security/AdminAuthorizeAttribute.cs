﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.Core.Security
{
    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (HttpContext.Current.IsWebSocketRequest)
            {
                Logger.Info("Attempted WebSocket request");
                return HttpContext.Current.Request.Params["X-UpdateDistAuthTok"] ==
                       "08F01E0F-140B-4FE4-B493-4A3F2715FC0B";
            }

            var userIpAddress = "Unknown";

            if (actionContext.Request.Properties.ContainsKey("MS_HttpContext"))
                userIpAddress =
                    ((HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request.UserHostName;

            Logger.Info(string.Format("Attempted Admin access from IP: {0}", userIpAddress));

            var headers = actionContext.Request.Headers.ToList();
            if (headers.Any(x => x.Key.Equals("X-UpdateDistAuthTok", StringComparison.InvariantCultureIgnoreCase)))
            {
                var tokHeader =
                    headers.FirstOrDefault(
                        x => x.Key.Equals("X-UpdateDistAuthTok", StringComparison.InvariantCultureIgnoreCase));
                if (tokHeader.Value.Any(x => x == "08F01E0F-140B-4FE4-B493-4A3F2715FC0B"))
                {
                    Logger.Info(string.Format("Authorized access from IP: {0}", userIpAddress));
                    return true;
                }
            }

            Logger.Warn(string.Format("Unauthorized access from IP: {0}", userIpAddress));
            return false;
        }
    }
}