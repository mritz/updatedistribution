﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.Core.Security
{
    public class ClientAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var userIpAddress = "Unknown";

            if (actionContext.Request.Properties.ContainsKey("MS_HttpContext"))
                userIpAddress =
                    ((HttpContextWrapper)actionContext.Request.Properties["MS_HttpContext"]).Request.UserHostName;

            Logger.Info(string.Format("Attempted Admin access from IP: {0}", userIpAddress));

            var headers = actionContext.Request.Headers.ToList();
            if (headers.Any(x => x.Key.Equals("X-UpdateDistAuthTok", StringComparison.InvariantCultureIgnoreCase)))
            {
                var tokHeader =
                    headers.FirstOrDefault(
                        x => x.Key.Equals("X-UpdateDistAuthTok", StringComparison.InvariantCultureIgnoreCase));
                if (tokHeader.Value.Any(x => x == "813DF262-2430-4384-8789-49652609FA54"))
                {
                    Logger.Info(string.Format("Authorized access from IP: {0}", userIpAddress));
                    return true;
                }
            }

            Logger.Warn(string.Format("Unauthorized access from IP: {0}", userIpAddress));
            return false;
        }
    }
}