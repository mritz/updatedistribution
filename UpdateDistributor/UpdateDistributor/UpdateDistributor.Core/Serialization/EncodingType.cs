﻿namespace UpdateDistributor.Core.Serialization
{
    public enum EncodingType
    {
        Unicode = 0,
        ASCII = 1,
        BigEndianUnicode = 2,
        Default = 3,
        UTF32 = 4,
        UTF7 = 5,
        UTF8 = 6
    }
}