﻿namespace UpdateDistributor.Core.Serialization
{
    public enum HashType
    {
        MD5 = 0,
        SHA1 = 1,
        PWDTK = 2
    }
}