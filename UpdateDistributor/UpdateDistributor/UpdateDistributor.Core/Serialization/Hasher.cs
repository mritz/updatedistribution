﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace UpdateDistributor.Core.Serialization
{
    public class Hasher : IDisposable
    {
        public Hasher(
            HashType algorithmToUse = HashType.MD5, EncodingType encodingToUse = EncodingType.Unicode,
            HashMode modeToUse = HashMode.InputString)
        {
            Algorithm = algorithmToUse;
            Encoding = encodingToUse;
            Mode = modeToUse;
        }
        public Hasher()
        {
            Algorithm = HashType.MD5;
            Encoding = EncodingType.Unicode;
            Mode = HashMode.InputString;
        }

        public HashType Algorithm { get; set; }
        public EncodingType Encoding { get; set; }
        public HashMode Mode { get; set; }

        public static string HashIt(
            HashType algorithmToUse, string input, EncodingType encodingToUse = EncodingType.ASCII,
            HashMode modeToUse = HashMode.InputString)
        {
            switch (modeToUse)
            {
                case HashMode.InputString:
                    return HashString(algorithmToUse, input);
                case HashMode.File:
                    return HashFile(input, algorithmToUse);
                default:
                    throw new ArgumentException("Invalid Hash Mode");
            }
        }
        public string HashThis(string input)
        {
            return HashIt(Algorithm, input, Encoding, Mode);
        }

        private static IEnumerable<byte> GetBytes(HashType algorithm, byte[] inBytes)
        {
            switch (algorithm)
            {
                case HashType.MD5:
                    using (var md5Hasher = MD5.Create())
                    {
                        return md5Hasher.ComputeHash(inBytes);
                    }
                case HashType.SHA1:
                    using (var sha1Hasher = SHA1.Create())
                    {
                        return sha1Hasher.ComputeHash(inBytes);
                    }
                default:
                    throw new ArgumentException("Invalid Algorithm Type");
            }
        }
        private static IEnumerable<byte> GetBytes(HashType algorithm, Stream inStream)
        {
            switch (algorithm)
            {
                case HashType.MD5:
                    using (var md5Er = MD5.Create())
                    {
                        return md5Er.ComputeHash(inStream);
                    }
                case HashType.SHA1:
                    using (var sha1Er = SHA1.Create())
                    {
                        return sha1Er.ComputeHash(inStream);
                    }
                default:
                    throw new ArgumentException("Invalid Algorithm Type");
            }
        }
        private static byte[] GetInputBytes(EncodingType encodingToUse, string input)
        {
            switch (encodingToUse)
            {
                case EncodingType.ASCII:
                    return System.Text.Encoding.ASCII.GetBytes(input);
                case EncodingType.BigEndianUnicode:
                    return System.Text.Encoding.BigEndianUnicode.GetBytes(input);
                case EncodingType.Default:
                    return System.Text.Encoding.Default.GetBytes(input);
                case EncodingType.Unicode:
                    return System.Text.Encoding.Unicode.GetBytes(input);
                case EncodingType.UTF32:
                    return System.Text.Encoding.UTF32.GetBytes(input);
                case EncodingType.UTF7:
                    return System.Text.Encoding.UTF7.GetBytes(input);
                case EncodingType.UTF8:
                    return System.Text.Encoding.UTF8.GetBytes(input);
                default:
                    throw new ArgumentException("Invalid Encoding Type Used");
            }
        }
        private static string HashFile(string fileName, HashType algorithToUse = HashType.MD5)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException(fileName);

            using (var file = new FileStream(fileName, FileMode.Open))
            {
                var retVal = GetBytes(algorithToUse, file);

                var sb = new StringBuilder();
                foreach (var t in retVal)
                {
                    sb.Append(t.ToString("x2"));
                }
                return sb.ToString();
            }
        }
        private static string HashString(
            HashType algorithmToUse, string input, EncodingType encodingToUse = EncodingType.ASCII)
        {
            var inputBytes = GetInputBytes(encodingToUse, input);

            var hashBytes = GetBytes(algorithmToUse, inputBytes);

            var sb = new StringBuilder();

            foreach (var t in hashBytes)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString();
        }

        #region IDisposable Members

        public void Dispose()
        {
            //this._algorithm = null;
            //this._encoding = null;
            //this._mode = null;
        }

        #endregion
    }
}