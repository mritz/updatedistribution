﻿using System.Collections.Generic;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Nuget;

namespace UpdateDistributor.Core.Utilities
{
    public interface IDeploymentHelper
    {
        bool DistributeNugetPackage(NugetDistribution nugetDistribution);
        Distribution GetActiveDistributionForEnvironment(int parcelId, int environment);
        IEnumerable<FileBundle> GetFileBundlesByParcelId(int parcelId);
        Parcel GetParcelById(int id);
        IEnumerable<Parcel> GetParcelsBySegmentId(int segmentId, string taxYear);
        IEnumerable<Segment> GetSegments();
        IEnumerable<SupportedEnvironment> GetSupportedForParcel(int parcelId);
        void UpdateDistribution(Distribution distribution);
        void UpdateParcel(Parcel parcel);
        void UpdateSupported(SupportedEnvironment supportedEnvironment);
    }
}