﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Text;

using UpdateDistributor.RestApi.Controllers;

namespace UpdateDistributor.RestApi.Authentication
{
    public sealed class AuthenticationModule
    {
        private const string CommunicationKey = "GQDstc21ewfffffffffffFiwDffVvVBrk";

        private readonly SecurityKey _signingKey =
            new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes(CommunicationKey));

        // The Method is used to generate token for user
        public static string GenerateTokenForUser(string userName, int userId)
        {
            var signingKey = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes(CommunicationKey));
            var now = DateTime.UtcNow;
            var signingCredentials = new SigningCredentials(
                signingKey,
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest);

            var claimsIdentity =
                new ClaimsIdentity(
                    new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, userName),
                        new Claim(ClaimTypes.NameIdentifier, userId.ToString())
                    },
                    "Custom");

            var securityTokenDescriptor = new SecurityTokenDescriptor
            {
                AppliesToAddress = "http://www.example.com",
                TokenIssuerName = "self",
                Subject = claimsIdentity,
                SigningCredentials = signingCredentials,
                Lifetime = new Lifetime(now, now.AddYears(1))
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
            var signedAndEncodedToken = tokenHandler.WriteToken(plainToken);

            return signedAndEncodedToken;
        }
        public static JwtAuthenticationIdentity PopulateUserIdentity(JwtSecurityToken userPayloadToken)
        {
            var name = userPayloadToken.Claims.FirstOrDefault(m => m.Type == "unique_name").Value;
            var userId = userPayloadToken.Claims.FirstOrDefault(m => m.Type == "nameid").Value;
            return new JwtAuthenticationIdentity(name) {UserId = Convert.ToInt32(userId), UserName = name};
        }
        /// Using the same key used for signing token, user payload is generated back
        public JwtSecurityToken GenerateUserClaimFromJwt(string authToken)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidAudiences = new[] {"http://www.example.com"},
                ValidIssuers = new[] {"self"},
                IssuerSigningKey = _signingKey
            };
            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            try
            {
                tokenHandler.ValidateToken(authToken, tokenValidationParameters, out validatedToken);
            }
            catch (Exception)
            {
                return null;
            }

            return validatedToken as JwtSecurityToken;
        }
    }
}