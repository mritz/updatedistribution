﻿using RFS.DataAccess.BusinessObject.UpdateDistributor;

namespace UpdateDistributor.RestApi.Authentication
{
    public interface IAuthenticationBackend
    {
        void Create(User user);
        User GetUser(string userName, string password);
    }
}