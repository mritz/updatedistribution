﻿using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

using UpdateDistributor.RestApi.Controllers;

namespace UpdateDistributor.RestApi.Authentication
{
    public class JwtAuthenticationFilterAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (!IsUserAuthorized(filterContext))
            {
                ShowAuthenticationError(filterContext);
                return;
            }
            base.OnAuthorization(filterContext);
        }

        private static string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null)
            {
                requestToken = authRequest.Parameter;
            }

            return requestToken;
        }
        private static bool IsUserAuthorized(HttpActionContext actionContext)
        {
            var authHeader = FetchFromHeader(actionContext);

            if (authHeader != null)
            {
                var auth = new AuthenticationModule();
                var userPayloadToken = auth.GenerateUserClaimFromJwt(authHeader);

                if (userPayloadToken != null)
                {
                    var identity = AuthenticationModule.PopulateUserIdentity(userPayloadToken);
                    string[] roles = {"All"};
                    var genericPrincipal = new GenericPrincipal(identity, roles);
                    Thread.CurrentPrincipal = genericPrincipal;
                    var authenticationIdentity = Thread.CurrentPrincipal.Identity as JwtAuthenticationIdentity;
                    if ((authenticationIdentity != null) && !string.IsNullOrEmpty(authenticationIdentity.UserName))
                    {
                        authenticationIdentity.UserId = identity.UserId;
                        authenticationIdentity.UserName = identity.UserName;
                    }
                    return true;
                }
            }
            return false;
        }
        private static void ShowAuthenticationError(HttpActionContext filterContext)
        {
            var responseDto = new ResponseDto {Code = 401, Message = "Unable to access, Please login again"};
            filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized, responseDto);
        }
    }
}