﻿using System.Security.Principal;

namespace UpdateDistributor.RestApi.Authentication
{
    public sealed class JwtAuthenticationIdentity : GenericIdentity
    {
        public JwtAuthenticationIdentity(string userName) : base(userName)
        {
            UserName = userName;
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}