﻿using System.Text;

using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Logging;

namespace UpdateDistributor.RestApi.Authentication
{
    public class LiveAuthenticationBackend : IAuthenticationBackend
    {
        private static LiveAuthenticationBackend _instance;

        private LiveAuthenticationBackend() {}

        public static LiveAuthenticationBackend Instance
        {
            get { return _instance ?? (_instance = new LiveAuthenticationBackend()); }
        }

        #region IAuthenticationBackend Members

        public void Create(User user)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Creating new user:");
            sb.AppendLine(string.Format("First Name: {0}",user.FirstName));
            sb.AppendLine(string.Format("Last Name: {0}",user.LastName));
            sb.AppendLine(string.Format("UserName: {0}",user.UserName));
            var message = sb.ToString();
            Logger.Info(message);
            var id = User.Save(user);
            if (id == -1)
            {
                Logger.Warn("Creating new user was unsuccessful.");
            }
        }
        public User GetUser(string userName, string password)
        {
            Logger.Info(string.Format("Retrieving user: {0}", userName));
            var user = User.Get(userName);

            if (user == null)
            {
                Logger.Warn("Could not locate user.");
            }
            if ((user != null) && (user.Password == password))
            {
                return user;
            }

            Logger.Warn("UserName/Password is incorrect.");
            return null;
        }

        #endregion
    }
}