﻿using System.Collections.Generic;
using System.Linq;

using RFS.DataAccess.BusinessObject.UpdateDistributor;

namespace UpdateDistributor.RestApi.Authentication
{
    public class MockAuthenticationBackend : IAuthenticationBackend
    {
        private static MockAuthenticationBackend _instance;

        private readonly List<User> _users = new List<User>();

        private MockAuthenticationBackend() {}

        public static MockAuthenticationBackend Instance
        {
            get { return _instance ?? (_instance = new MockAuthenticationBackend()); }
        }

        #region IAuthenticationBackend Members

        public void Create(User user)
        {
            _users.Add(user);
        }
        public User GetUser(string userName, string password)
        {
            var user = _users.FirstOrDefault(x => x.UserName == userName);
            if ((user != null) && (user.Password == password))
            {
                return user;
            }
            return null;
        }

        #endregion
    }
}