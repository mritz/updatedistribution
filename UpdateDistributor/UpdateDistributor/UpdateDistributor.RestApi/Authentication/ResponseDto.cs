﻿namespace UpdateDistributor.RestApi.Authentication
{
    internal sealed class ResponseDto
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}