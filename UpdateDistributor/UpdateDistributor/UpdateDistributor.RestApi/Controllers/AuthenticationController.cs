﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.RestApi.Authentication;

namespace UpdateDistributor.RestApi.Controllers
{
    public class AuthenticationController : ApiController
    {
        private readonly IAuthenticationBackend _authenticationBackend;

        public AuthenticationController()
        {
            _authenticationBackend = LiveAuthenticationBackend.Instance;
        }

        [Route("api/Authentication/Login/{username}/{password}")]
        [HttpPost]
        public HttpResponseMessage Login(string username, string password)
        {
            Logger.Info(string.Format("User '{0}' is attempting to log in", username));
            var user = _authenticationBackend.GetUser(username, password);
            
            if (user == null)
            {
                return Request.CreateResponse(
                    HttpStatusCode.Unauthorized,
                    "UserName/Password is incorrect",
                    Configuration.Formatters.JsonFormatter);
            }

            var token = AuthenticationModule.GenerateTokenForUser(user.UserName, user.Id);
            Logger.Info(string.Format("Login successful. Token created: {0}", token));
            user.Token = token;
            return Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(user));
        }
    }
}