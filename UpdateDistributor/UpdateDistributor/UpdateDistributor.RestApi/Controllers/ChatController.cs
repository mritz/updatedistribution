﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using Microsoft.Web.WebSockets;

namespace UpdateDistributor.RestApi.Controllers
{
    public class ChatController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get()
        {
            if (HttpContext.Current.IsWebSocketRequest)
            {
                HttpContext.Current.AcceptWebSocketRequest(new ChatWebSocketHandler());
                return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }

    public class ChatWebSocketHandler : WebSocketHandler
    {
        private static readonly WebSocketCollection ChatClients = new WebSocketCollection();

        public override void OnClose()
        {
            ChatClients.Remove(this);
        }
        public override void OnMessage(string message)
        {
            ChatClients.Broadcast(message);
        }
        public override void OnOpen()
        {
            ChatClients.Add(this);
        }
    }
}