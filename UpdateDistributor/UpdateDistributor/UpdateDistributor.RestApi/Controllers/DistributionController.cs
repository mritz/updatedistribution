﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using Microsoft.Web.WebSockets;

using NuGet;

using RCS.Common.Compression;
using RCS.Common.Serialization;

using RFS.DataAccess.BusinessObject.UpdateDistributor;

using Syncfusion.Data.Extensions;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Nuget;
using UpdateDistributor.Core.Security;
using UpdateDistributor.Core.Utilities;
using UpdateDistributor.RestApi.Deployment;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class DistributionController : ApiController
    {
        private static readonly object SyncObject = new object();

        [HttpGet]
        public IEnumerable<Distribution> Get()
        {
            Logger.Info("Retrieving all distributions");
            List<Distribution> distributions = null;
            try
            {
                distributions = Distribution.GetAll().ToList();
                Logger.Info(string.Format("Retrieved {0} distributions", distributions.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return distributions;
        }
        [Route("api/Distribution/{parcelId}/{environment}")]
        [HttpGet]
        public Distribution Get(int parcelId, int environment)
        {
            Logger.Info(
                string.Format("Retrieving {0} distribution for parcel {1}", (ReleaseLevel)environment, parcelId));
            Distribution distribution = null;
            try
            {
                distribution = Distribution.GetCurrentForEnvironment(parcelId, environment);
                if (distribution != null)
                {
                    Logger.Info(
                        string.Format(
                            "Retrieved {1} distribution with ID: {0}",
                            distribution.Id,
                            (ReleaseLevel)environment));
                }
                else
                {
                    Logger.Info(
                        string.Format(
                            "No {1} distribution found for parcel ID: {0}",
                            parcelId,
                            (ReleaseLevel)environment));
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return distribution;
        }
        [HttpPost]
        public Distribution Post([FromBody] Distribution distribution)
        {
            Logger.Info("Creating distribution");
            try
            {
                var id = Distribution.Save(distribution);
                distribution.Id = id;
                Logger.Info(string.Format("Created distribution with ID: {0}", distribution.Id));
                return distribution;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
        [Route("api/Distribution/PromoteDistribution/{parcelId}/{environment}")]
        [HttpPut]
        public void PromoteDistribution(int parcelId, int environment)
        {
            
            new TaskFactory().StartNew(
                () =>
                {
                    lock (SyncObject)
                    {
                        Logger.Info(
                            string.Format(
                                "Promoting distribution to {0} for parcel {1}",
                                (ReleaseLevel)environment,
                                parcelId));
                        
                        IDeploymentHelper deploymentHelper = new DeploymentHelper();
                        var parcel = deploymentHelper.GetParcelById(parcelId);
                        var fromDistribution = deploymentHelper.GetActiveDistributionForEnvironment(
                            parcelId,
                            environment - 1);
                        var toDistribution = new Distribution
                        {
                            Id = -1,
                            ParcelId = parcelId,
                            Environment = environment,
                            Release = DateTime.Now,
                            Version = fromDistribution.Version,
                            Active = true
                        };

                        var nugetPackage = new NugetController().GetDistribution(
                            (ReleaseLevel)(environment - 1),
                            parcel.FileNameOfParcel.Replace(".zip", string.Empty),
                            SemanticVersion.Parse(fromDistribution.Version));
                        var success = PushNugetPackage(
                            parcel.TaxYear,
                            nugetPackage,
                            parcel,
                            toDistribution,
                            deploymentHelper);

                        if (success)
                        {
                            deploymentHelper.UpdateDistribution(toDistribution);
                        }
                        else
                        {
                            throw new Exception("Unable to distribute Nuget package.");
                        }

                        Logger.Info(
                            string.Format(
                                "Finished promoting distribution to {0} for parcel {1}",
                                (ReleaseLevel)environment,
                                parcelId));

                        OnDistributionPromoted(toDistribution);
                    }
                });
        }
        [HttpPut]
        public Distribution Put([FromBody] Distribution distribution)
        {
            Logger.Info(string.Format("Updating distribution with ID: {0}", distribution.Id));
            try
            {
                Distribution.Save(distribution);
                Logger.Info(string.Format("Updated distribution with ID: {0}", distribution.Id));
                return distribution;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
        [Route("api/Distribution/DistributionPromotedWebSocket")]
        [HttpGet]
        public HttpResponseMessage DistributionPromotedWebSocket()
        {
            if (HttpContext.Current.IsWebSocketRequest)
            {
                var webSocketHandler = new PromoteDistributionWebSocketHandler();
                HttpContext.Current.AcceptWebSocketRequest(webSocketHandler);
                return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        private static void OnDistributionPromoted(Distribution distribution)
        {
            DistributionPromoted?.Invoke(
                typeof(DistributionController),
                new DistributionPromotedEventArgs(distribution));
        }

        private static bool PushNugetPackage(
            string taxYear, IPackage nugetPackage, Parcel parcel, Distribution distribution,
            IDeploymentHelper deploymentHelper)
        {
            var tempDirectory = Path.Combine(Path.GetTempPath(), string.Format("Temp{0}", parcel.Id));

            try
            {
                if (Directory.Exists(tempDirectory))
                {
                    Directory.Delete(tempDirectory, true);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }

            nugetPackage.ExtractContents(new PhysicalFileSystem(@"C:\"), tempDirectory);
            var zipper = new Zipper(Path.Combine(Path.GetTempPath(), "nuget.zip"));

            NugetHelper.FlushZipArchive(zipper);

            var libLocation = Path.Combine(tempDirectory, "lib");
            foreach (var file in Directory.GetFiles(libLocation))
            {
                try
                {
                    var bytes = File.ReadAllBytes(file);
                    zipper.AppendToZipFile(Path.GetFileName(file), bytes);
                }
                catch (Exception e)
                {
                    Logger.Error(string.Format("Unable to add {0} to archive.", file), e);
                }
            }

            var zipBytes = File.ReadAllBytes(zipper.ZipLocation);
            var nugetDistribution = new NugetDistribution(taxYear, parcel, distribution, zipBytes);
            var success = deploymentHelper.DistributeNugetPackage(nugetDistribution);

            NugetHelper.FlushZipArchive(zipper);
            return success;
        }

        public static event EventHandler<DistributionPromotedEventArgs> DistributionPromoted;
    }

    public class DistributionPromotedEventArgs : EventArgs
    {
        public DistributionPromotedEventArgs(Distribution distribution)
        {
            Distribution = distribution;
        }

        public Distribution Distribution { get; set; }
    }

    public class PromoteDistributionWebSocketHandler : WebSocketHandler
    {
        private static readonly WebSocketCollection Clients = new WebSocketCollection();

        public override void OnClose()
        {
            Clients.Remove(this);
            DistributionController.DistributionPromoted -= OnDistributionPromoted;
        }
        //public override void OnMessage(string message)
        //{
        //    Clients.Broadcast(message);
        //}
        public override void OnOpen()
        {
            Clients.Add(this);
            DistributionController.DistributionPromoted += OnDistributionPromoted;
        }

        private void OnDistributionPromoted(object sender, DistributionPromotedEventArgs e)
        {
            var json = ObjectJsonSerializer<Distribution>.Serialize(e.Distribution);
            Clients.Broadcast(json);
        }
    }
}