﻿using System.Web.Http;
using System.Web.Mvc;

namespace UpdateDistributor.RestApi.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult FileSize()
        {
            Response.StatusCode = 500;
            Response.StatusDescription = "Maximum file size exceeded";
            Response.End();
            return null;
        }
    }
}