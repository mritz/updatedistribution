using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class FileBundleController : ApiController
    {
        public static IEnumerable<FileBundle> GetByParcelId(Parcel parcel)
        {
            return FileBundle.GetByParcelId(parcel.Id);
        }
        [HttpDelete]
        public bool Delete(int id)
        {
            Logger.Info(string.Format("Deleting file bundle {0}", id));
            try
            {
                FileBundle.Delete(id);
                Logger.Info(string.Format("Deleted file bundle {0}", id));
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return false;
            }
        }
        [HttpGet]
        public IEnumerable<FileBundle> Get()
        {
            Logger.Info("Retrieving all file bundles");
            List<FileBundle> fileBundles = null;
            try
            {
                fileBundles = FileBundle.GetAll().ToList();
                Logger.Info(string.Format("Retrieved {0} file bundles", fileBundles.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return fileBundles;
        }
        [HttpGet]
        public FileBundle Get(int id)
        {
            Logger.Info(string.Format("Retrieving file bundle {0}", id));
            FileBundle fileBundle = null;
            try
            {
                fileBundle = FileBundle.GetById(id);
                Logger.Info(string.Format("Retrieved file bundle {0}", id));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return fileBundle;
        }
        [Route("api/FileBundle/GetByParcelId/{parcelId}")]
        [HttpGet]
        public IEnumerable<FileBundle> GetByParcelId(int parcelId)
        {
            Logger.Info(string.Format("Retrieving file bundles for parcel {0}", parcelId));
            List<FileBundle> fileBundles = null;
            try
            {
                fileBundles = FileBundle.GetByParcelId(parcelId).ToList();
                Logger.Info(string.Format("Retrieved {0} file bundles", fileBundles.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return fileBundles;
        }
        [HttpPost]
        public FileBundle Post([FromBody] FileBundle fileBundle)
        {
            Logger.Info("Creating file bundle");
            try
            {
                var id = FileBundle.Save(fileBundle);
                fileBundle.Id = id;
                Logger.Info(string.Format("Create file bundle with ID: {0}", fileBundle.Id));
                return fileBundle;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
        [HttpPut]
        public FileBundle Put([FromBody] FileBundle fileBundle)
        {
            Logger.Info(string.Format("Updating file bundle with ID: {0}", fileBundle.Id));
            try
            {
                FileBundle.Save(fileBundle);
                Logger.Info(string.Format("Updated file bundle with ID: {0}", fileBundle.Id));
                return fileBundle;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
    }
}