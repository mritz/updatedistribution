﻿using System;
using System.IO;
using System.Web.Http;

using NuGet;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Client.Nuget;
using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Nuget;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public sealed class NugetController : ApiController
    {
        [Route("api/Nuget/DistributeNugetPackage")]
        [HttpPost]
        public bool DistributeNugetPackage([FromBody] NugetDistribution nugetDistribution)
        {
            if (nugetDistribution == null)
            {
                Logger.Warn("Argument cannot be null");
                throw new ArgumentNullException(nameof(nugetDistribution));
            }
            if (nugetDistribution.Parcel == null)
            {
                Logger.Warn("Parcel cannot be null");
                throw new ArgumentException("Parcel cannot be null", nameof(nugetDistribution));
            }
            if (nugetDistribution.Distribution == null)
            {
                Logger.Warn("Distribution cannot be null");
                throw new ArgumentException("Distribution cannot be null", nameof(nugetDistribution));
            }

            var tempDirectory = Path.Combine(Path.GetTempPath(), string.Format("Temp{0}", nugetDistribution.Parcel.Id));

            try
            {
                NugetHelper.CleanStagingDirectory(tempDirectory);
                var stagingDirectory = NugetHelper.BuildStagingDirectory(tempDirectory);
                var zipFile = NugetHelper.CreateArchive(nugetDistribution.ZipBytes, stagingDirectory);
                NugetHelper.UnpackArchiveToStagingDirectory(stagingDirectory, zipFile);

                if (nugetDistribution.Parcel.KeyFilePath == null)
                {
                    Logger.Warn("Parcel KeyFilePath is null. Cannot complete operation.");
                    return false;
                }

                var nugetManifest = NugetHelper.CreateNugetManifest(
                    nugetDistribution.Parcel.FileNameOfParcel,
                    nugetDistribution.Distribution.Version,
                    stagingDirectory);
                var packageFileName = NugetHelper.CreateNugetPackage(
                    nugetDistribution.Parcel.FileNameOfParcel,
                    nugetManifest,
                    stagingDirectory);
                NugetHelper.PushPackageToNugetServer(
                    nugetDistribution.Parcel.FileNameOfParcel,
                    (ReleaseLevel)nugetDistribution.Distribution.Environment,
                    stagingDirectory,
                    packageFileName);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return false;
            }
            finally
            {
                NugetHelper.CleanStagingDirectory(tempDirectory);
            }
        }
        public IPackage GetDistribution(ReleaseLevel environment, string packageId, SemanticVersion version)
        {
            var client = new NugetClient(string.Format("{0}/nuget", NugetHelper.GetNugetServer(environment)));
            var package = client.FindPackage(packageId, version);
            return package;
        }
    }
}