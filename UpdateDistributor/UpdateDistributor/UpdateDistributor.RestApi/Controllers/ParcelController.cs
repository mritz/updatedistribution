﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class ParcelController : ApiController
    {
        [HttpGet]
        public IEnumerable<Parcel> Get()
        {
            Logger.Info("Retrieving all parcels");
            List<Parcel> parcels = null;
            try
            {
                parcels = Parcel.GetAll().ToList();
                Logger.Info(string.Format("Found {0} parcels", parcels.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return parcels;
        }
        [HttpGet]
        public Parcel Get(int id)
        {
            Logger.Info(string.Format("Retrieving parcel {0}", id));
            Parcel parcel = null;
            try
            {
                parcel = Parcel.GetById(id);
                Logger.Info(string.Format("Retrieved parcel {0}", id));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return parcel;
        }
        [Route("api/Parcel/GetBySegmentId/{segmentId}/{taxYear}")]
        [HttpGet]
        public IEnumerable<Parcel> GetBySegmentId(int segmentId, string taxYear)
        {
            Logger.Info(string.Format("Retrieving parcels for segment {0} from tax year {1}", segmentId, taxYear));
            List<Parcel> parcels = null;
            try
            {
                parcels = Parcel.GetBySegmentId(segmentId, taxYear).ToList();
                Logger.Info(
                    string.Format(
                        "Retrieved {0} parcels for segment {1} from tax year {2}",
                        parcels.Count,
                        segmentId,
                        taxYear));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return parcels;
        }
        [HttpPost]
        public Parcel Post([FromBody] Parcel parcel)
        {
            Logger.Info("Creating parcel");
            try
            {
                var id = Parcel.Save(parcel);
                parcel.Id = id;
                Logger.Info(string.Format("Created parcel with ID: {0}", parcel.Id));
                return parcel;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
        [HttpPut]
        public Parcel Put([FromBody] Parcel parcel)
        {
            Logger.Info(string.Format("Updating parcel with ID: {0}", parcel.Id));
            try
            {
                Parcel.Save(parcel);
                Logger.Info(string.Format("Updated parcel with ID: {0}", parcel.Id));
                return parcel;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
    }
}