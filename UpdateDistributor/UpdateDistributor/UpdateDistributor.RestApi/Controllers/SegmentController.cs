﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class SegmentController : ApiController
    {
        public IEnumerable<Segment> Get()
        {
            Logger.Info("Retrieving all segments");
            List<Segment> segments = null;
            try
            {
                segments = Segment.GetAll().ToList();
                Logger.Info(string.Format("Retrieved {0} segments", segments.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return segments;
        }
        public Segment Get(int id)
        {
            Logger.Info(string.Format("Retrieving segment with ID: {0}", id));
            Segment segment = null;
            try
            {
                segment = Segment.GetById(id);
                Logger.Info(string.Format("Retrieved segment with ID: {0}", id));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return segment;
        }
    }
}