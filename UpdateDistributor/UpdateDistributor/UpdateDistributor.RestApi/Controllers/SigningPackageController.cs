﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;

using NuGet;

using RCS.Common.Compression;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Models;
using UpdateDistributor.Core.Nuget;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class SigningPackageController : ApiController
    {
        [Route("api/SigningPackage/{taxYear}")]
        [HttpGet]
        public IEnumerable<SigningPackage> Get(string taxYear)
        {
            Logger.Info("Retrieving all pending signing packages");
            var signingPackages = new List<SigningPackage>();
            var parcels = Parcel.GetAll().Where(x => x.TaxYear == taxYear);

            foreach (var parcel in parcels)
            {
                try
                {
                    var distribution = Distribution.GetCurrentForEnvironment(parcel.Id, (int)ReleaseLevel.Production);
                    if (distribution != null && !distribution.Signed)
                    {
                        var package = new NugetController().GetDistribution(
                            ReleaseLevel.Production,
                            parcel.FileNameOfParcel.Replace(".zip", string.Empty),
                            SemanticVersion.Parse(distribution.Version));
                        var extractPath = Path.Combine(Path.GetTempPath(), package.Id);
                        if (Directory.Exists(extractPath))
                        {
                            Directory.Delete(extractPath, true);
                        }
                        package.ExtractContents(new PhysicalFileSystem(@"C:\"), extractPath);
                        var zipDirectory = Path.Combine(extractPath, "lib");
                        var zipFile = Path.Combine(zipDirectory, string.Format("{0}.zip", package.Id));
                        var zipper = new Zipper(zipFile);
                        zipper.ZipDirectory(zipDirectory);
                        var bytes = File.ReadAllBytes(zipFile);
                        signingPackages.Add(new SigningPackage(taxYear, distribution, bytes));
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message, e);
                }
            }

            Logger.Info(string.Format("Retrieved {0} pending signing packages for {1}", signingPackages.Count, taxYear));
            return signingPackages;
        }
        [HttpPut]
        public bool Put([FromBody] SigningPackage signingPackage)
        {
            Logger.Info(
                string.Format("Updating distribution {0} with signed assemblies", signingPackage.Distribution.Id));
            try
            {
                Distribution.Save(signingPackage.Distribution);
                Logger.Info(
                    string.Format("Updated distribution {0} with signed assemblies", signingPackage.Distribution.Id));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return false;
            }

            var parcel = Parcel.GetById(signingPackage.Distribution.ParcelId);
            var tempDirectory = Path.Combine(Path.GetTempPath(), string.Format("Temp{0}", parcel.Id));

            try
            {
                NugetHelper.CleanStagingDirectory(tempDirectory);
                var stagingDirectory = NugetHelper.BuildStagingDirectory(tempDirectory);
                var zipFile = NugetHelper.CreateArchive(signingPackage.ZipBytes, stagingDirectory);
                NugetHelper.UnpackArchiveToStagingDirectory(stagingDirectory, zipFile);

                if (parcel.KeyFilePath == null)
                {
                    Logger.Warn("Parcel KeyFilePath is null. Cannot complete operation.");
                    return false;
                }

                var nugetManifest = NugetHelper.CreateNugetManifest(
                    parcel.FileNameOfParcel,
                    signingPackage.Distribution.Version,
                    stagingDirectory);
                var packageFileName = NugetHelper.CreateNugetPackage(
                    parcel.FileNameOfParcel,
                    nugetManifest,
                    stagingDirectory);
                NugetHelper.PushPackageToNugetServer(
                    parcel.FileNameOfParcel,
                    (ReleaseLevel)signingPackage.Distribution.Environment,
                    stagingDirectory,
                    packageFileName);

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return false;
            }
            finally
            {
                NugetHelper.CleanStagingDirectory(tempDirectory);
            }
        }
    }
}