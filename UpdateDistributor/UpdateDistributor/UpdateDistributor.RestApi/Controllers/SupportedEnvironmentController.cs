﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using TaxProgram.Infrastructure.Attributes.Modules;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class SupportedEnvironmentController : ApiController
    {
        private static readonly object SyncObject = new object();

        [HttpGet]
        [Route("api/SupportedEnvironment/{parcelId}")]
        public IEnumerable<SupportedEnvironment> Get(int parcelId)
        {
            Logger.Info("Retrieving all supported environments");
            List<SupportedEnvironment> supportedEnvironments = null;
            try
            {
                supportedEnvironments = SupportedEnvironment.GetByParcelId(parcelId).ToList();
                Logger.Info(string.Format("Retrieved {0} supported environments", supportedEnvironments.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return supportedEnvironments;
        }
        [Route("api/SupportedEnvironment/{parcelId}/{environment}")]
        [HttpGet]
        public SupportedEnvironment Get(int parcelId, int environment)
        {
            Logger.Info(
                string.Format(
                    "Retrieving {0} supported environment for parcel {1}",
                    (ReleaseLevel)environment,
                    parcelId));
            SupportedEnvironment supportedEnvironment = null;
            try
            {
                supportedEnvironment = SupportedEnvironment.GetByParcelIdForEnvironment(parcelId, environment);
                if (supportedEnvironment != null)
                {
                    Logger.Info(
                        string.Format(
                            "Retrieved {1} supported environment with ID: {0}",
                            supportedEnvironment.Id,
                            (ReleaseLevel)environment));
                }
                else
                {
                    Logger.Info(
                        string.Format(
                            "No {1} supported environment found for parcel ID: {0}",
                            parcelId,
                            (ReleaseLevel)environment));
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return supportedEnvironment;
        }
        [HttpPost]
        public SupportedEnvironment Post([FromBody] SupportedEnvironment supportedEnvironment)
        {
            Logger.Info("Creating supported environment");
            try
            {
                var id = SupportedEnvironment.Save(supportedEnvironment);
                supportedEnvironment.Id = id;
                Logger.Info(string.Format("Created supported environment with ID: {0}", supportedEnvironment.Id));
                return supportedEnvironment;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
        [HttpPut]
        public SupportedEnvironment Put([FromBody] SupportedEnvironment supportedEnvironment)
        {
            Logger.Info(string.Format("Updating supported environment with ID: {0}", supportedEnvironment.Id));
            try
            {
                SupportedEnvironment.Save(supportedEnvironment);
                Logger.Info(string.Format("Updated supported environment with ID: {0}", supportedEnvironment.Id));
                return supportedEnvironment;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
                return null;
            }
        }
    }
}