﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Logging;
using UpdateDistributor.Core.Security;

namespace UpdateDistributor.RestApi.Controllers
{
    [AdminAuthorize]
    public class TaxYearController : ApiController
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            Logger.Info("Retrieving all tax years");
            List<string> taxYears = null;
            try
            {
                taxYears = Parcel.GetAll().Select(parcel => parcel.TaxYear).Distinct().ToList();
                Logger.Info(string.Format("Retrieved {0} tax years", taxYears.Count));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, e);
            }
            return taxYears;
        }
    }
}