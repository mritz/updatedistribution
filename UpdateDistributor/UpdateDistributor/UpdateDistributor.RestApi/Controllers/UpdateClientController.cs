﻿using System.Collections.Generic;
using System.Web.Http;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Client.Enumerations;
using UpdateDistributor.Core.Security;

using StatusReport = UpdateDistributor.Client.Models.StatusReport;

namespace UpdateDistributor.RestApi.Controllers
{
    [ClientAuthorize]
    public class UpdateClientController : ApiController
    {
        [HttpGet]
        public IEnumerable<StatusReport> GetStatusReports(string taxYear, int environment)
        {
            var segments = Segment.GetAll();
            foreach (var segment in segments)
            {
                var parcels = Parcel.GetBySegmentId(segment.Id, taxYear);
                foreach (var parcel in parcels)
                {
                    var supported = SupportedEnvironment.GetByParcelIdForEnvironment(parcel.Id, environment);
                    var distribution = Distribution.GetCurrentForEnvironment(parcel.Id, environment);
                    if (distribution != null)
                    {
                        yield return
                            new StatusReport
                            {
                                AssociatedButton = (UpdateType)segment.Id,
                                Changes = supported?.Entities,
                                FilenameOfPackage = parcel.FileNameOfParcel,
                                ID = parcel.Id,
                                KeyFilePath = parcel.KeyFilePath,
                                KeyFileVersion = distribution?.Version,
                                Release = distribution?.Release
                            };
                    }
                }
            }
        }
    }
}