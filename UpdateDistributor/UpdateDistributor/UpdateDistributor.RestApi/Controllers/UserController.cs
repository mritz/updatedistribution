﻿using System.Web.Http;

using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.RestApi.Authentication;

namespace UpdateDistributor.RestApi.Controllers
{
    public class UserController : ApiController
    {
        private readonly IAuthenticationBackend _authenticationBackend;

        public UserController()
        {
            _authenticationBackend = LiveAuthenticationBackend.Instance;
        }

        [HttpPost]
        public void Create([FromBody] User user)
        {
            _authenticationBackend.Create(user);
        }
    }
}