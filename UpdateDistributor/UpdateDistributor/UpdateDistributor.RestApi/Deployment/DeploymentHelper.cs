﻿using System;
using System.Collections.Generic;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Nuget;
using UpdateDistributor.Core.Utilities;
using UpdateDistributor.RestApi.Controllers;

namespace UpdateDistributor.RestApi.Deployment
{
    public class DeploymentHelper : IDeploymentHelper
    {
        #region IDeploymentHelper Members

        public bool DistributeNugetPackage(NugetDistribution nugetDistribution)
        {
            return new NugetController().DistributeNugetPackage(nugetDistribution);
        }
        public Distribution GetActiveDistributionForEnvironment(int parcelId, int environment)
        {
            return new DistributionController().Get(parcelId, environment);
        }
        public IEnumerable<FileBundle> GetFileBundlesByParcelId(int parcelId)
        {
            return new FileBundleController().GetByParcelId(parcelId);
        }
        public Parcel GetParcelById(int id)
        {
            return new ParcelController().Get(id);
        }
        public IEnumerable<Parcel> GetParcelsBySegmentId(int segmentId, string taxYear)
        {
            return new ParcelController().GetBySegmentId(segmentId, taxYear);
        }
        public IEnumerable<Segment> GetSegments()
        {
            return new SegmentController().Get();
        }
        public IEnumerable<SupportedEnvironment> GetSupportedForParcel(int parcelId)
        {
            return new SupportedEnvironmentController().Get(parcelId);
        }
        public void UpdateDistribution(Distribution distribution)
        {
            var controller = new DistributionController();
            distribution = distribution.Id == -1 ? controller.Post(distribution) : controller.Put(distribution);
            if (distribution == null)
                throw new InvalidOperationException("Distribution table not updated.");
        }
        public void UpdateParcel(Parcel parcel)
        {
            parcel = new ParcelController().Put(parcel);
            if (parcel == null)
                throw new InvalidOperationException("Parcel table not updated.");
        }
        public void UpdateSupported(SupportedEnvironment supportedEnvironment)
        {
            var controller = new SupportedEnvironmentController();
            supportedEnvironment = supportedEnvironment.Id == -1
                                       ? controller.Post(supportedEnvironment)
                                       : controller.Put(supportedEnvironment);
            if (supportedEnvironment == null)
                throw new InvalidOperationException("Supported table not updated.");
        }

        #endregion
    }
}