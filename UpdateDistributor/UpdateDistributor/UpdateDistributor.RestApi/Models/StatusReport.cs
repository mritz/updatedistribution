﻿using System;

using UpdateDistributor.Core.Enumerations;

namespace UpdateDistributor.RestApi.Models
{
    public class StatusReport
    {
        public StatusReport()
        {
            ID = -1;
        }

        public UpdateType AssociatedButton { get; set; }
        public string Changes { get; set; }
        public string FilenameOfPackage { get; set; }
        public int ID { get; set; }
        public string KeyFilePath { get; set; }
        public string KeyFileVersion { get; set; }
        public bool Mandatory { get; set; }
        public bool OutOfDate { get; set; }
        public string PackageHash { get; set; }
        public DateTime? Release { get; set; }
    }
}