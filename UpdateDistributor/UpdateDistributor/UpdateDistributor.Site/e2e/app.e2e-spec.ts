import { UpdateDistributorPage } from './app.po';

describe('update-distributor App', () => {
  let page: UpdateDistributorPage;

  beforeEach(() => {
    page = new UpdateDistributorPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
