import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            console.log(`AuthGuard: ${localStorage.getItem('currentUser')} logged in.`);
            return true;
        }

        // not logged in so redirect to login page with the return url
        console.log('AuthGuard: No user currently logged in. Redirecting to /login');
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}