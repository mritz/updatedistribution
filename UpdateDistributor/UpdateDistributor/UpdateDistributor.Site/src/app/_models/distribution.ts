export interface Distribution {
    active: boolean;
    changes: string;
    environment: number;
    id: number;
    parcelId: number;
    release: Date;
    supported: string;
    version: string;
    isPromoting: boolean;
}