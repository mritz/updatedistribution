export interface FileBundle{
    buildPath: string;
    fileName: string;
    filePath: string;
    fileType: number;
    id: number;
    parcelId: number;
}