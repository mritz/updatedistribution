export interface Parcel {
    fileNameOfParcel: string;
    id: number;
    keyFilePath: string;
    segmentId: number;
    parcelHash: string;
    taxYear: number;
}
