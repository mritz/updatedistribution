import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { UpdateDistributionService } from '../_services/update-distribution.service';

@Injectable()
export class AuthenticationService extends UpdateDistributionService {
    private _url: string;

    constructor(private httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}Authentication`;
    }

    login(username: string, password: string) {
        let url = `${this._url}/Login/${username}/${password}`;

        return this.httpClient.post(url, null, { headers: this.GetHttpHeaders(false) })
            .map(response => {
                let user = JSON.parse(response.toString());
                // login successful if there's a jwt token in the response
                console.log('AuthenticationService: Login successful.');
                console.log('User: ' + user.UserName);
                console.log('Token: ' + user.Token);
                if (user && user.Token && user.Access == 1) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}