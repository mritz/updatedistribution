import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { WebSocketService } from "./web-socket.service";

const CHAT_URL = 'ws://localhost:49613/api/chat';

export interface Message {
    author: string,
    message: string
}

@Injectable()
export class ChatService {
    public messages: Subject<Message>;

    constructor(webSocketService: WebSocketService) {
        this.messages = <Subject<Message>>webSocketService
            .connect(CHAT_URL)
            .map((response: MessageEvent): Message => {
                let data = JSON.parse(response.data);
                return {
                    author: data.author,
                    message: data.message
                }
            });
    }
}