import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { WebSocketService } from "./web-socket.service";

import { environment } from '../../environments/environment';
import { Distribution } from "../_models";

// @Injectable()
// export class DistributionWebSocketService {
//     distributions: Subject<Distribution[]>;

//     private _baseUrl = environment.serviceUrl
//         .replace("http:\\", "ws:\\")
//         .replace("https:\\", "wss:\\");

//     constructor(
//         private webSocketService: WebSocketService) { }

//     loadAll() {
//         this.distributions = <Subject<Distribution[]>>this.webSocketService
//             .connect(this._baseUrl)
//             .map((response: MessageEvent): Distribution => {
//                 let data = JSON.parse(response.data);
//             });
//         return data;
//     });
// }
// }