import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/throw';

import { UpdateDistributionService } from './update-distribution.service';
import { WebSocketService } from './web-socket.service';

import { Distribution } from '../_models/index';

@Injectable()
export class DistributionService extends UpdateDistributionService {
    distributions: Observable<Distribution[]>;
    historyDistributions: Observable<Distribution[]>;
    lastUpdatedDistribution: Subject<Distribution>;

    private _distributions: BehaviorSubject<Distribution[]>;
    private _historyDistributions: BehaviorSubject<Distribution[]>;
    private _url: string;
    private _dataStore: {
        distributions: Distribution[],
        historyDistributions: Distribution[]
    };

    constructor(
        private webSocketService: WebSocketService,
        httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}Distribution`;
        this._dataStore = { distributions: [], historyDistributions: [] };
        this._distributions = <BehaviorSubject<Distribution[]>>new BehaviorSubject([]);
        this.distributions = this._distributions.asObservable();
        this._historyDistributions = <BehaviorSubject<Distribution[]>>new BehaviorSubject([]);
        this.historyDistributions = this._historyDistributions.asObservable();

        this.subscribeToPromotions();
        this.lastUpdatedDistribution.subscribe(distribution => {
            let found: boolean = false;
            distribution.isPromoting = false;
            
            this._dataStore.distributions.forEach((t, i) => {
                if (t.parcelId == distribution.parcelId && t.environment == distribution.environment) {
                    this._dataStore.distributions[i] = distribution;
                    found = true;
                    console.log(`Distribution ${distribution.id} updated.`);
                }
            });

            if (!found) {
                this._dataStore.distributions.push(distribution);
                console.log(`Distribution ${distribution.id} created.`);
            }
            this._distributions.next(Object.assign({}, this._dataStore).distributions);
        })
    }

    loadAll() {
        let url = this._url;
        let options = this.GetHttpHeaders(false);

        this._httpClient.get<Distribution[]>(url, { headers: options })
            .subscribe(data => {
                let distributions = new Array<Distribution>();
                data.forEach((element: any) => {
                    if (element != null) {
                        let distribution = this.convertClrToDistribution(element);
                        distributions.push(distribution);
                    }
                });
                this._dataStore.distributions = distributions;
                this._distributions.next(Object.assign({}, this._dataStore).distributions);
            }, error => console.log("Could not load distributions."));
    }

    loadAllActive() {
        let url = this._url;
        let options = this.GetHttpHeaders(false);

        this._httpClient.get<Distribution[]>(url, { headers: options })
            .subscribe(data => {
                let distributions = new Array<Distribution>();
                data.forEach((element: any) => {
                    if (element != null) {
                        let distribution = this.convertClrToDistribution(element);
                        distributions.push(distribution);
                    }
                });
                this._dataStore.distributions = distributions;
                this._distributions.next(Object.assign({}, this._dataStore).distributions);
            }, error => console.log("Could not load distributions."));
    }

    loadHistoryForParcelId(parcelId: number, environment: number) {
        let url = this._url;
        let options = this.GetHttpHeaders(false);

        this._httpClient.get<Distribution[]>(url, { headers: options })
            .subscribe(data => {
                let distributions = new Array<Distribution>();
                data.forEach((element: any) => {
                    if (element != null) {
                        let distribution = this.convertClrToDistribution(element);
                        if (distribution.parcelId == parcelId && distribution.environment == environment) {
                            distributions.push(distribution);
                        }
                    }
                });
                this._dataStore.historyDistributions = distributions.sort((a, b) => {
                    if (a.release < b.release) return 1;
                    if (a.release == b.release) return 0;
                    if (a.release > b.release) return -1;
                });
                this._historyDistributions.next(Object.assign({}, this._dataStore).historyDistributions);
            }, error => console.log("Could not load history distributions."));
    }

    promoteDistribution(environment: number, parcelId: number) {
        let url = `${this._url}/PromoteDistribution/${parcelId}/${environment}`;
        let body = undefined;
        let options = this.GetHttpHeaders(false);

        this._dataStore.distributions.forEach((t, i) => {
            if (t.parcelId == parcelId && t.environment == environment) {
                t.isPromoting = true;
                this._dataStore.distributions[i] = t;
            }
        });
        this._distributions.next(Object.assign({}, this._dataStore).distributions);

        this._httpClient.put(url, body, { headers: options })
            .toPromise()
            .then(() => null)
            .catch((reason) => {
                console.log("Could not promote distribution." + reason);
            });
    }

    subscribeToPromotions() {
        let url = `${this._url}/DistributionPromotedWebSocket`
            .replace("http://", "ws://")
            .replace("https://", "wss://");

        this.lastUpdatedDistribution =
            <Subject<Distribution>>this.webSocketService
                .connect(url)
                .map((response: MessageEvent): Distribution => {
                    let distribution = this.convertClrToDistribution(JSON.parse(response.data));
                    return distribution;
                }, error => {
                    console.log("Could not promote distribution." + error);
                });
    }

    setActiveDistribution(distribution: Distribution) {
        let url = this._url;
        let body = JSON.stringify(this.convertDistributionToClr(distribution));
        let options = this.GetHttpHeaders(true);

        this._httpClient.put(url, body, { headers: options })
            .subscribe(data => {
                let distribution: Distribution = this.convertClrToDistribution(data);
                this._dataStore.distributions.forEach((t, i) => {
                    if (t.id == distribution.id) {
                        this._dataStore.distributions[i] = distribution;
                    }
                });

                this._distributions.next(Object.assign({}, this._dataStore).distributions);
            }, error => console.log("Could not update distribution."));
    }

    private convertClrToDistribution(element: any): Distribution {
        let distribution = <Distribution>{
            id: element.Id,
            parcelId: element.ParcelId,
            supported: element.Changes,
            environment: element.Environment,
            release: element.Release,
            version: element.Version,
            active: element.Active,
            isPromoting: element.IsPromoting
        };
        return distribution;
    }

    private convertDistributionToClr(distribution: Distribution) {
        let clr = {
            Id: distribution.id,
            ParcelId: distribution.parcelId,
            Supported: distribution.supported,
            Environment: distribution.environment,
            Release: distribution.release,
            Version: distribution.version,
            Active: distribution.active,
            IsPromoting: distribution.isPromoting
        };
        return clr;
    }
}