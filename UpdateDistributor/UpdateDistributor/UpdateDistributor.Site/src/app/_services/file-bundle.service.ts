import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { UpdateDistributionService } from './update-distribution.service';

import { FileBundle } from '../_models/index';

@Injectable()
export class FileBundleService extends UpdateDistributionService {
    fileBundles: Observable<FileBundle[]>;

    private _fileBundles: BehaviorSubject<FileBundle[]>;
    private _url: string;
    private _dataStore: {
        fileBundles: FileBundle[]
    };

    constructor(httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}FileBundle`;
        this._dataStore = { fileBundles: [] };
        this._fileBundles = <BehaviorSubject<FileBundle[]>>new BehaviorSubject([]);
        this.fileBundles = this._fileBundles.asObservable();
    }

    create(fileBundle: FileBundle) {
        let url = this._url;
        let body = JSON.stringify(this.convertFileBundleToClr(fileBundle));
        let options = this.GetHttpHeaders(true);

        this._httpClient.post(url, body, {headers:options})
            .subscribe(data => {
                this._dataStore.fileBundles.push(this.convertClrToFileBundle(data));
                this._fileBundles.next(Object.assign({}, this._dataStore).fileBundles);
            }, error => console.log("Could not create file bundle."));
    }

    loadAll() {
        let url = this._url;
        let options = this.GetHttpHeaders(false);

        this._httpClient.get<FileBundle[]>(url, {headers:options})
            .subscribe(data => {
                let fileBundles = new Array<FileBundle>();
                data.forEach((element: any) => {
                    if (element != null) {
                        let fileBundle = this.convertClrToFileBundle(element);
                        fileBundles.push(fileBundle);
                    }
                });
                this._dataStore.fileBundles = fileBundles;
                this._fileBundles.next(Object.assign({}, this._dataStore).fileBundles);
            }, error => console.log("Could not load file bundles."));
    }

    remove(fileBundleId: number) {
        let url = `${this._url}?id=${fileBundleId}`
        let options = this.GetHttpHeaders(true);

        this._httpClient.delete(url, {headers: options})
            .subscribe(response => {
                this._dataStore.fileBundles.forEach((fileBundle: any, index: number) => {
                    if (fileBundle.id == fileBundleId) {
                        this._dataStore.fileBundles.splice(index, 1);
                    }
                });
                this._fileBundles.next(Object.assign({}, this._dataStore).fileBundles);
            }, error => console.log("Could not delete file bundle."));
    }

    update(fileBundle: FileBundle) {
        let url = `${this._url}?id=${fileBundle.id}`;
        let body = JSON.stringify(this.convertFileBundleToClr(fileBundle));
        let options = this.GetHttpHeaders(true);

        this._httpClient.put(url, body, {headers: options})
            .subscribe(data => {
                let updatedFileBundle = this.convertClrToFileBundle(data);
                this._dataStore.fileBundles.forEach((fileBundle: any, index: number) => {
                    if (fileBundle.id == updatedFileBundle.id) {
                        this._dataStore.fileBundles[index] = updatedFileBundle;
                    }
                });
                this._fileBundles.next(Object.assign({}, this._dataStore).fileBundles);
            }, error => console.log("Could not update file bundle."));
    }

    private convertClrToFileBundle(element: any): FileBundle {
        let fileBundle = <FileBundle>{
            id: element.Id,
            parcelId: element.ParcelId,
            buildPath: element.BuildPath,
            fileName: element.FileName,
            filePath: element.FilePath,
            fileType: element.FileType
        };
        return fileBundle;
    }

    private convertFileBundleToClr(fileBundle: FileBundle): any {
        let clr = {
            Id: fileBundle.id,
            ParcelId: fileBundle.parcelId,
            BuildPath: fileBundle.buildPath,
            FileName: fileBundle.fileName,
            FilePath: fileBundle.filePath,
            FileType: fileBundle.fileType
        };
        return clr;
    }
}