import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UpdateDistributionService } from './update-distribution.service';

import { Parcel } from '../_models/index';

@Injectable()
export class ParcelService extends UpdateDistributionService {
    private _parcels: BehaviorSubject<Parcel[]>;
    private _url: string;
    private _dataStore: {
        parcels: Parcel[]
    };

    constructor(httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}Parcel`;
        this._dataStore = { parcels: [] };
        this._parcels = <BehaviorSubject<Parcel[]>>new BehaviorSubject([]);
    }

    get parcels(){
        return this._parcels.asObservable();
    }

    loadAll() {
        let url = this._url;
        let options = this.GetHttpHeaders(false);

        this._httpClient.get<Parcel[]>(url, {headers:options})
            .subscribe(data => {
                let parcels = new Array<Parcel>();
                data.forEach((element: any) => {
                    if (element != null) {
                        let parcel = this.convertClrToParcel(element);
                        parcels.push(parcel);
                    }
                });
                this._dataStore.parcels = parcels;
                this._parcels.next(Object.assign({}, this._dataStore).parcels);
            }, error => console.log("Could not load parcels."));
    }

    private convertClrToParcel(element: any): Parcel {
        let parcel = <Parcel>{
            id: element.Id,
            segmentId: element.SegmentId,
            fileNameOfParcel: element.FileNameOfParcel,
            keyFilePath: element.KeyFilePath,
            parcelHash: element.ParcelHash,
            taxYear: element.TaxYear
        };
        return parcel;
    }
}
