import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UpdateDistributionService } from './update-distribution.service';

import { Segment } from '../_models/index';

@Injectable()
export class SegmentService extends UpdateDistributionService {
    private _segments: BehaviorSubject<Segment[]>;
    private _url: string;
    private _dataStore: {
        segments: Segment[]
    };

    constructor(httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}Segment`;
        this._dataStore = { segments: [] };
        this._segments = <BehaviorSubject<Segment[]>>new BehaviorSubject([]);
    }

    get segments(){
        return this._segments.asObservable();
    }

    loadAll(taxYear: string) {
        let url = this._url;
        let options = this.GetHttpHeaders(false);

        this._httpClient.get<Segment[]>(url, { headers: options })
            .subscribe(data => {
                let segments = new Array<Segment>();
                data.forEach((element: any) => {
                    if (element != null) {
                        let segment = this.convertClrToSegment(element);
                        segments.push(segment);
                    }
                });
                this._dataStore.segments = segments;
                this._segments.next(Object.assign({}, this._dataStore).segments);
            }, error => console.log("Could not load segments"));
    }

    private convertClrToSegment(element: any): Segment {
        let segment = <Segment>{
            id: element.Id,
            name: element.Name,
            stateAbbreviation: element.StateAbbreviation
        };
        return segment;
    }
}
