import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { UpdateDistributionService } from './update-distribution.service';

@Injectable()
export class TaxYearService extends UpdateDistributionService {
    private _url: string;

    constructor(
        private httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}TaxYear`;
    }

    getTaxYears(): Observable<string[]> {
        return this.httpClient.get<string[]>(this._url, { headers: this.GetHttpHeaders(false) });
    }
}