import { environment } from '../../environments/environment';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable()
export class UpdateDistributionService {
    protected _baseUrl = environment.serviceUrl;

    constructor(
        protected _httpClient: HttpClient
    ) { }

    GetHttpHeaders(jsonType: boolean): HttpHeaders {
        let httpHeaders = new HttpHeaders();
        if (jsonType) {
            httpHeaders = httpHeaders.append('Content-Type', 'application/json');
        }

        httpHeaders = httpHeaders.append('timeout', '2000000000');
        httpHeaders = httpHeaders.append('X-UpdateDistAuthTok', '08F01E0F-140B-4FE4-B493-4A3F2715FC0B');
        return httpHeaders;
    }
}