import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';
import { UpdateDistributionService } from '../_services/update-distribution.service';

@Injectable()
export class UserService extends UpdateDistributionService {
    private _url: string;

    constructor(private httpClient: HttpClient) {
        super(httpClient);
        this._url = `${this._baseUrl}User`;
    }

    getAll() {
        return this.httpClient.get<User[]>(this._url);
    }

    getById(id: number) {
        return this.httpClient.get(this._url + id);
    }

    create(user: User) {
        return this.httpClient.post(this._url, user);
    }

    update(user: User) {
        return this.httpClient.put(this._url + user.id, user);
    }

    delete(id: number) {
        return this.httpClient.delete(this._url + id);
    }
}