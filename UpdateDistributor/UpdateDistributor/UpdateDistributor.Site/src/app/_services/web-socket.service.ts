import { Injectable } from "@angular/core";

import * as Rx from 'rxjs/Rx';

@Injectable()
export class WebSocketService {
    constructor() { }

    private subject: Rx.Subject<MessageEvent>;

    public connect(url): Rx.Subject<MessageEvent> {
        if (!this.subject) {
            this.subject = this.create(url);
            console.log("Successfully connected: " + url);
        }
        return this.subject;
    }

    create(url): Rx.Subject<MessageEvent> {
        let authUrl = `${url}?X-UpdateDistAuthTok=08F01E0F-140B-4FE4-B493-4A3F2715FC0B`
        let ws = new WebSocket(authUrl);

        let observable = Rx.Observable.create(
            (obs: Rx.Observer<MessageEvent>) => {
                ws.onmessage = obs.next.bind(obs);
                ws.onerror = obs.error.bind(obs);
                ws.onclose = obs.complete.bind(obs);
                return ws.close.bind(ws);
            });

        let observer = {
            next: (data: Object) => {
                if (ws.readyState == WebSocket.OPEN) {
                    ws.send(JSON.stringify(data));
                }
            }
        }

        return Rx.Subject.create(observer, observable);
    }
}