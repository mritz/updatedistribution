import { environment } from '../environments/environment';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './_guards/index'
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { HomeComponent } from 'app/home/home.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: environment.useHash })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }