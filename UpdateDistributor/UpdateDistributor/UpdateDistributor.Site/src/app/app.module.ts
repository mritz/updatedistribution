import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/* fake backend */
// import { fakeBackendProvider } from './_helpers/index';

/* Core Components */
import { AppComponent } from './app.component';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor } from './_helpers/index';

/* Feature Modules */
import { HomeModule } from './home/home.module';
import { SegmentModule } from './segments/segment.module';
import { ParcelModule } from './parcels/parcel.module';
import { DistributionModule } from './distributions/distribution.module';
import { FileBundleModule } from './file-bundles/file-bundle.module';

/* Route Components */
import { AppRoutingModule } from './app-routing.module';

/* Services */
import { AlertService, AuthenticationService, TaxYearService, UserService } from './_services/index';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    HomeModule,
    SegmentModule,
    ParcelModule,
    DistributionModule,
    FileBundleModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    RegisterComponent
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    TaxYearService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    // fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
