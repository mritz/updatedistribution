import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService, Message } from '../_services/chat.service';

@Component({
    selector: 'ud-chat',
    templateUrl: './chat.component.html'
})
export class ChatComponent implements OnInit, OnDestroy {
    activeMessage: Message;
    newMessage: string;

    constructor(private chatService: ChatService) { }

    // private message = {
    //     author: 'test author',
    //     message: 'test message'
    // }

    sendMsg() {
        let newPackage = {
            author: "newAuthor",
            message: this.newMessage
        };
        console.log('new message from client to websocket: ', newPackage);
        this.chatService.messages.next(newPackage);
    }

    ngOnInit(): void {
        this.chatService.messages.subscribe(msg => {
            console.log("Response from websocket: " + msg);
            this.activeMessage = {
                author: msg.author,
                message: msg.message
            };
        });
    }
    ngOnDestroy(): void {
        this.chatService.messages.unsubscribe();
    }
}