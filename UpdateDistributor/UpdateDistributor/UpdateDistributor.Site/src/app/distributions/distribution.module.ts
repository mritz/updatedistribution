import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { 
    DistributionListComponent, 
    DistributionSummaryComponent, 
    DistributionHistoryComponent
} from './index';
import { DistributionService } from '../_services/index';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule
    ],
    exports: [
        DistributionListComponent
    ],
    declarations: [
        DistributionListComponent,
        DistributionSummaryComponent,
        DistributionHistoryComponent
    ],
    providers: [
        DistributionService
    ]
})
export class DistributionModule { }
