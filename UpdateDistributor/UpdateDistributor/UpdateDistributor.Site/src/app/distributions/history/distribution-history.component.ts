import { Component, Input, OnInit } from '@angular/core';
import { FormsModule, NgModel } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { Distribution, Parcel } from '../../_models/index';
import { DistributionService, ParcelService } from '../../_services/index';

@Component({
    selector: "ud-distribution-history",
    templateUrl: './distribution-history.component.html'
})
export class DistributionHistoryComponent implements OnInit {
    errorMessage: string;
    @Input() parcelId: number;
    @Input() environment: number;
    @Input() distributions: Observable<Distribution[]>;

    constructor(
        private _parcelService: ParcelService,
        private _distributionService: DistributionService
    ) { }

    ngOnInit(): void {
        let distributions = this.distributions;
        // this.distributions = this._distributionService.distributions;

        // this._distributionService.loadHistoryForParcelId(this.parcelId, this.environment);
    }

    onSelectionChanged(distribution: Distribution){
        // distribution.active = true;
        // this._distributionService.setActiveDistribution(distribution);
    }
}