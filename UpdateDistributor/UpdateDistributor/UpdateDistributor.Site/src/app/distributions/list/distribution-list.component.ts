import { Component, Input, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Distribution } from '../../_models/index';
import { DistributionService } from '../../_services/index';
import { ParcelDetailComponent } from 'app/parcels';

@Component({
    selector: "ud-distribution-list",
    templateUrl: './distribution-list.component.html',
    styleUrls: ['./distribution-list.component.css']
})
export class DistributionListComponent implements OnInit {
    @Input() parent: ParcelDetailComponent;
    isCollapsed: boolean = false;
    isHistoryCollapsed: boolean = true;
    @Input() parcelId: number;
    devDistribution: Observable<Distribution>;
    qaDistribution: Observable<Distribution>;
    prodDistribution: Observable<Distribution>;
    historyDistributions: Observable<Distribution[]>;

    constructor(
        private _distributionService: DistributionService
    ) { }

    ngOnInit(): void {
        this.devDistribution = this._distributionService.distributions
            .map(distributions => distributions.find(item =>
                item.parcelId == this.parcelId && item.environment == 0 && item.active));
        this.qaDistribution = this._distributionService.distributions
            .map(distributions => distributions.find(item =>
                item.parcelId == this.parcelId && item.environment == 1 && item.active));
        this.prodDistribution = this._distributionService.distributions
            .map(distributions => distributions.find(item =>
                item.parcelId == this.parcelId && item.environment == 2 && item.active));
    }

    promoteDistribution(environment: number) {
        this._distributionService.promoteDistribution(environment, this.parcelId);
    }
}