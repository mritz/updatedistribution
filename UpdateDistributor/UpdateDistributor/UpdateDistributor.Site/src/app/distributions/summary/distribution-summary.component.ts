import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

import { Distribution } from '../../_models/index';
import { DistributionService } from '../../_services/index';

@Component({
    selector: "ud-distribution-summary",
    templateUrl: './distribution-summary.component.html',
    styleUrls: ['./distribution-summary.component.css']
})
export class DistributionSummaryComponent {
    errorMessage: string;
    @Input() distribution: Distribution;
    @Output() onClick: EventEmitter<any> = new EventEmitter();

    constructor(
        private _router: Router,
        private _distributionService: DistributionService
    ) { }

    navigateTo(): void {
        this.onClick.emit();
        // this._router.navigate(['/distribution/', this.distribution.parcelId, this.distribution.environment]);
        // return true;
    }
}