import { Component, Input, Output, EventEmitter } from '@angular/core';

import { FileBundle } from '../../_models/index';
import { FileBundleService } from '../../_services/index';

@Component({
    selector: "ud-file-bundle-delete",
    templateUrl: './file-bundle-delete.component.html'
})
export class FileBundleDeleteComponent {
    @Input() fileBundle: FileBundle;
    @Output() canceled: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private _fileBundleService: FileBundleService
    ) { }

    onCancel() {
        this.canceled.emit();
    }

    onSubmit() {
        this._fileBundleService.remove(this.fileBundle.id);
    }
}