import { Component, Input } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { NgbAccordion, NgbPanelChangeEvent, NgbPanel } from '@ng-bootstrap/ng-bootstrap';

import { FileBundle } from '../../_models/index';
import { FileBundleService } from '../../_services/index';

@Component({
    selector: "ud-file-bundle-detail",
    templateUrl: './file-bundle-detail.component.html'
})
export class FileBundleDetailComponent {
    @Input() fileBundle: FileBundle;
    isDeleting: boolean;
    isEditing: boolean;

    constructor(
        private _fileBundleService: FileBundleService
    ) { }
}