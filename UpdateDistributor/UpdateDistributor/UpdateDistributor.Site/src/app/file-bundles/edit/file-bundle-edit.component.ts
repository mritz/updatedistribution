import { Component, Input, Output, EventEmitter } from '@angular/core';

import { FileBundle } from '../../_models/index';
import { FileBundleService } from '../../_services/index';

@Component({
    selector: "ud-file-bundle-edit",
    templateUrl: './file-bundle-edit.component.html'
})
export class FileBundleEditComponent {
    @Input() fileBundle: FileBundle;
    @Output() canceled: EventEmitter<void> = new EventEmitter<void>();
    @Output() created: EventEmitter<FileBundle> = new EventEmitter<FileBundle>();

    fileTypes: any[] = [
        { id: 0, name: "Assembly version for dll files" },
        { id: 1, name: "Hash value for file" },
        { id: 2, name: "Version string on update parcel" },
        { id: 3, name: "Assembly Symbols" },
        { id: 4, name: "Data lookup" }
    ];

    constructor(
        private _fileBundleService: FileBundleService
    ) { }

    onCancel() {
        this.canceled.emit();
    }

    onSubmit() {
        if (this.fileBundle.id >= 0) {
            this._fileBundleService.update(this.fileBundle);
        }
        else {
            this._fileBundleService.create(this.fileBundle);
            this.created.emit();
        }
    }
}