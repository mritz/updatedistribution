import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { 
    FileBundleListComponent,
    FileBundleDeleteComponent,
    FileBundleDetailComponent,
    FileBundleEditComponent
} from './index';
import { FileBundleService } from '../_services/index';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule
    ],
    exports: [
        FileBundleListComponent
    ],
    declarations: [
        FileBundleListComponent,
        FileBundleDeleteComponent,
        FileBundleDetailComponent,
        FileBundleEditComponent
    ],
    providers: [
        FileBundleService
    ]
})
export class FileBundleModule { }
