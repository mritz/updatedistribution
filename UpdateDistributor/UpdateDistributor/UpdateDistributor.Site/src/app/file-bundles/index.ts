export * from './delete/file-bundle-delete.component';
export * from './detail/file-bundle-detail.component';
export * from './edit/file-bundle-edit.component';
export * from './list/file-bundle-list.component';