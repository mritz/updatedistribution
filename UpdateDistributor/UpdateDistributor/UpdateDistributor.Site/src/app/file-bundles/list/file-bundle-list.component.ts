import { Component, Input, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { FileBundle } from '../../_models/index';
import { FileBundleService } from '../../_services/index';

@Component({
    selector: "ud-file-bundle-list",
    templateUrl: './file-bundle-list.component.html'
})
export class FileBundleListComponent implements OnInit {
    isCreating: boolean;
    @Input() parcelId: number;
    newFileBundle: FileBundle;
    fileBundles: Observable<FileBundle[]>;

    constructor(
        private _fileBundleService: FileBundleService
    ) { }

    ngOnInit(): void {
        this.fileBundles = this._fileBundleService.fileBundles
            .map(fileBundles => fileBundles.filter(item =>
                item.parcelId == this.parcelId));

        this._fileBundleService.loadAll();
        this.isCreating = false;
        this.resetNewFileBundle();
    }

    onCreated() {
        this.resetNewFileBundle();
    }

    resetNewFileBundle() {
        this.isCreating = false;
        this.newFileBundle = <FileBundle>{
            buildPath: "",
            fileName: "",
            filePath: "",
            fileType: 0,
            id: -1,
            parcelId: this.parcelId
        };
    }
}