import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';

import { SegmentListComponent } from '../segments/index';
import { DistributionService, TaxYearService } from '../_services/index';
import { User } from '../_models/index';
import { DistributionListComponent } from 'app/distributions';
import { ParcelDetailComponent } from 'app/parcels';

@Component({
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
    isChecked: boolean;
    currentUser: any;
    taxYears: string[];
    errorMessage: string;
    @ViewChildren(SegmentListComponent) segmentListComponents: SegmentListComponent[];

    constructor(
        private _router: Router,
        private _taxYearService: TaxYearService,
        private _distributionService: DistributionService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit(): void {
        this._taxYearService.getTaxYears()
            .subscribe(taxYears => this.taxYears = taxYears,
                error => this.errorMessage = <any>error);
    }

    toggleChildren() {
        this.segmentListComponents.forEach(segmentList => {
            segmentList.segmentDetailComponents.forEach(segmentDetail => {
                segmentDetail.isChecked = this.isChecked;
                segmentDetail.toggleChildren();
            })
        })
    }

    promoteDistributions(environment: number) {
        this.segmentListComponents.forEach(segmentList => {
            segmentList.segmentDetailComponents.forEach(segmentDetail => {
                segmentDetail.parcelListComponents.forEach(parcelList => {
                    parcelList.parcelDetailComponents.forEach(parcelDetail => {
                        if (parcelDetail.isChecked) {
                            parcelDetail.distributionListComponents.forEach(distributions => {
                                distributions.promoteDistribution(environment);
                            });
                        }
                    });
                });
            });
        });
    }
}
