import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SegmentModule } from '../segments/segment.module';
import { HomeComponent } from '../home/home.component';
import { TaxYearService } from '../_services/index';
import { ChatComponent } from '../chat/chat.component';
import { ChatService } from '../_services/chat.service';
import { WebSocketService } from '../_services/web-socket.service';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule,
        SegmentModule,
        RouterModule
    ],
    exports: [
        HomeComponent,
        ChatComponent
    ],
    declarations: [
        HomeComponent,
        ChatComponent
    ],
    providers: [
        WebSocketService,
        ChatService,
        TaxYearService
    ]
})
export class HomeModule { }