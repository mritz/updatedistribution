import { Component, Input, QueryList, ViewChildren } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { NgbAccordion, NgbPanelChangeEvent, NgbPanel } from '@ng-bootstrap/ng-bootstrap';

import { Parcel } from '../../_models/index';
import { ParcelService } from '../../_services/index';
import { DistributionListComponent } from '../../distributions/index';

@Component({
    selector: "ud-parcel-detail",
    templateUrl: './parcel-detail.component.html',
    styleUrls: ['./parcel-detail.component.css']
})
export class ParcelDetailComponent {
    @Input() parcel: Parcel;
    isChecked: boolean;
    isCollapsed: boolean = false;
    @ViewChildren(DistributionListComponent) distributionListComponents: QueryList<DistributionListComponent>;

    constructor(
        private _parcelService: ParcelService
    ) { }

    accordionChange($event: NgbPanelChangeEvent) {
        this.isCollapsed = $event.nextState;
        this.distributionListComponents.forEach(element => {
            if (element.parcelId == +$event.panelId.replace(/^\D+/g, '')) {
                element.isCollapsed = $event.nextState;
            }
        });
    }
}
