import { Component, Input, OnInit, ViewChildren } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Parcel } from '../../_models/index';
import { ParcelService } from '../../_services/index';
import { ParcelDetailComponent } from '../index';

@Component({
    selector: "ud-parcel-list",
    templateUrl: './parcel-list.component.html'
})
export class ParcelListComponent implements OnInit {
    @Input() segmentId: number;
    @Input() taxYear: number;
    parcels: Observable<Parcel[]>;
    @ViewChildren(ParcelDetailComponent) parcelDetailComponents: ParcelDetailComponent[];

    constructor(
        private _parcelService: ParcelService
    ) { }

    ngOnInit(): void {
        this.parcels = this._parcelService.parcels
            .map(parcels => parcels.filter(item =>
                item.segmentId == this.segmentId && item.taxYear == this.taxYear));
    }
}
