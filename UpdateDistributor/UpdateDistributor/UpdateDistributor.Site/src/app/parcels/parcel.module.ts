import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ParcelDetailComponent, ParcelListComponent } from './index';
import { ParcelService } from '../_services/index';
import { DistributionModule } from '../distributions/distribution.module';
import { FileBundleModule } from '../file-bundles/file-bundle.module';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule,
        DistributionModule,
        FileBundleModule
    ],
    exports: [
        ParcelListComponent
    ],
    declarations: [
        ParcelListComponent,
        ParcelDetailComponent
    ],
    providers: [
        ParcelService
    ]
})
export class ParcelModule { }
