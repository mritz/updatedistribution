import { Component, Input, ViewChildren } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NgbAccordion, NgbPanel } from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs/Observable';

import { Segment } from '../../_models/index';
import { ParcelListComponent } from '../../parcels/index';

@Component({
    selector: 'ud-segment-detail',
    templateUrl: './segment-detail.component.html'
})
export class SegmentDetailComponent {
    errorMessage: string;
    isChecked: boolean;
    @Input() segment: Segment;
    @Input() taxYear: string;
    @ViewChildren(ParcelListComponent) parcelListComponents: ParcelListComponent[];

    toggleChildren() {
        this.parcelListComponents.forEach(parcelList => {
            parcelList.parcelDetailComponents.forEach(parcelDetail => {
                parcelDetail.isChecked = this.isChecked;
            })
        });
    }
}