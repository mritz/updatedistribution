import { Component, Input, OnInit, ViewChildren } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NgbAccordion, NgbPanel } from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs/Observable';

import { Segment } from '../../_models/index';
import { SegmentDetailComponent } from '../index';
import { DistributionService, ParcelService, SegmentService } from '../../_services/index';

@Component({
    selector: 'ud-segment-list',
    templateUrl: './segment-list.component.html'
})
export class SegmentListComponent implements OnInit {
    errorMessage: string;
    @Input() taxYear: string;
    segments: Observable<Segment[]>;
    @ViewChildren(SegmentDetailComponent) segmentDetailComponents: SegmentDetailComponent[];

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _segmentService: SegmentService,
        private _parcelService: ParcelService,
        private _distributionService: DistributionService
    ) { }

    ngOnInit(): void {
        this.segments = this._segmentService.segments;

        this._segmentService.loadAll(this.taxYear);
        this._parcelService.loadAll();
        this._distributionService.loadAll();
    }
}
