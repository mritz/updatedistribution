import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SegmentService } from '../_services/index';
import { SegmentDetailComponent, SegmentListComponent } from './index';
import { ParcelModule } from '../parcels/parcel.module';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule,
        ParcelModule
    ],
    exports: [
        SegmentListComponent,
        SegmentDetailComponent
    ],
    declarations: [
        SegmentListComponent,
        SegmentDetailComponent
    ],
    providers: [
        SegmentService
    ]
})
export class SegmentModule { }
