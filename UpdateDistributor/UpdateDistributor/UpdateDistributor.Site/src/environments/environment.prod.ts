export const environment = {
  production: false,
  serviceUrl: "https://distributor.taxslayerpro.com/api/api/",
  useHash: false
};
