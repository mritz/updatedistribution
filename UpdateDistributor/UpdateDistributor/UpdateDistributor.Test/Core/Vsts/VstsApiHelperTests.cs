﻿using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using UpdateDistributor.BuildWatch.Vsts;

namespace UpdateDistributor.Test.Core.Vsts
{
    [TestClass]
    public class VstsApiHelperTests
    {
        [TestMethod]
        public void Builds()
        {
            const int expectedCount = 2;

            var builds =
                VstsBuildsQuery.Create("pro", BuildType.Xaml)
                               .WithDefinition("TaxSlayer2017")
                               .Top(expectedCount)
                               .Execute()
                               .Result;

            Assert.AreEqual(expectedCount, builds.Count());
        }
    }
}