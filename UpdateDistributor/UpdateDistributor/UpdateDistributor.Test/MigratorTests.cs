﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using UpdateDistributor.Core.Utility;

namespace UpdateDistributor.Test
{
    [TestClass]
    public sealed class MigratorTests
    {
        [TestMethod]
        public void Migrate()
        {
            Migrator.Migrate(2016, 2017);
        }
    }
}