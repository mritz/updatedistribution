﻿using System;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using RCS.Common.Compression;

using RFS.DataAccess.BusinessObject.ProOperations;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

using UpdateDistributor.Core.Nuget;
using UpdateDistributor.RestApi.Controllers;

namespace UpdateDistributor.Test.RestApi.Controllers
{
    [TestClass]
    public class NugetControllerTests
    {
        [TestMethod]
        public void CanCreateSpecFile()
        {
            const string zipFile = "nuget.zip";
            var zipper = new Zipper(zipFile);

            zipper.ZipDirectory("./TestData/Nuget/");

            var parcel = new Parcel
            {
                Id = 760,
                SegmentId = 10,
                FileNameOfParcel = "GA.DLL.zip",
                TaxYear = "2017",
                KeyFilePath = @".\States\GA\States.GA.Database.dll"
            };

            var distribution = new Distribution
            {
                Id = 1,
                ParcelId = 760,
                Version = "1.0.0.0",
                Environment = 0,
                Release = DateTime.Now
            };

            var nugetController = new NugetController();
            var nugetDistribution = new NugetDistribution("2017", parcel, distribution, File.ReadAllBytes(zipFile));

            nugetController.DistributeNugetPackage(nugetDistribution);
        }
    }
}