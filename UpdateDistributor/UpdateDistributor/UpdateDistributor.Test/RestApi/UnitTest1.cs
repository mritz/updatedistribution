﻿using System;
using System.Diagnostics;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UpdateDistributor.Test.RestApi
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var localRepo = @"\\pro-build-01\Builds\10\pro\OCTOPUS-IrsSchema-2017\bin";
            var apiKey = "3382FF13-2B62-4375-B335-C87BCDB07C44";
            var nugetServer = "https://distributor.taxslayerpro.com/qa";
            RunCommandLine(
                "nuget.exe",
                string.Format(
                    "push {0} {1} {2} {3} {4}",
                    Path.Combine(localRepo, "Schema.2017.Deployment.2017.10.3.2.nupkg"),
                    apiKey,
                    "-verbosity detailed",
                    "-timeout 6001",
                    string.Format("-Source {0}/api/v2/package/", nugetServer)));
        }

        private void RunCommandLine(string command, string arguments)
        {
            var processStartInfo = new ProcessStartInfo(command)
            {
                Arguments = arguments,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = false,
                WindowStyle = ProcessWindowStyle.Normal
            };

            var process = new Process();
            process.StartInfo = processStartInfo;
            process.OutputDataReceived += Process_OutputDataReceived;
            process.Start();

            var output = process.StandardOutput.ReadToEnd();
            Console.WriteLine(output);
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }
    }
}