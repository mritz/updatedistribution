﻿using System.Windows;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using UpdateDistributor.Core;

namespace UpdateDistributor
{
    public class Bootstrapper:UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            var resolve = Container.Resolve<MainWindow>();
            return resolve;

        }

        protected override void InitializeShell()
        {
            base.InitializeShell();
            Application.Current.MainWindow = (Window)Shell;
            Application.Current.MainWindow.Show();

        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<NavigationService>();
        }
    }
}