﻿using System.Linq;
using Microsoft.Practices.Prism.Regions;

namespace UpdateDistributor.Core
{
    public class NavigationService
    {
        private readonly IRegionManager _regionManager;
        private IRegion ContentRegion { get { return _regionManager.Regions[RegionNames.ContentRegion]; } }

        public NavigationService(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void GoTo(object view)
        {
            ContentRegion.Add(view);
            ContentRegion.Activate(view);
        }

        public void GoBack()
        {
            var view = ContentRegion.Views.LastOrDefault();
            if (view != null)
            {
                ContentRegion.Remove(view);
            }
        }
    }
}