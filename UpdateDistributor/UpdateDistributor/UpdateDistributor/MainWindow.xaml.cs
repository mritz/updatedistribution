﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using UpdateDistributor.Core;
using UpdateDistributor.Views;

namespace UpdateDistributor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {

        public MainWindow(IUnityContainer unityContainer)
        {
            InitializeComponent();
            var regionManager = unityContainer.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion(RegionNames.ContentRegion, typeof(DistributionView));
        }
    }
}
