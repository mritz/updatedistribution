﻿using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;

namespace UpdateDistributor.Models
{
    public class UpdateItem : NotificationObject
    {
        private string _qaVer;
        private string _releaseVer;
        public string ParcelName { get; set; }

        public string DevVer { get; set; }

        public string QAVer
        {
            get { return _qaVer; }
            set
            {
                if (value == _qaVer) return;
                _qaVer = value;
                RaisePropertyChanged(() => QAVer);
            }
        }

        public string ReleaseVer
        {
            get { return _releaseVer; }
            set
            {
                if (value == _releaseVer) return;
                _releaseVer = value;
                RaisePropertyChanged(() => ReleaseVer);
            }
        }

        public int ParcelId { get; set; }

        public ICommand DeployToQaCommand { get { return new DelegateCommand<UpdateItem>(PushToQa); } }
        public ICommand DeployToProductionCommand { get { return new DelegateCommand<UpdateItem>(PushToProduction); } }

        private static void PushToQa(UpdateItem item)
        {
            var a = item;
        }
        private static void PushToProduction(UpdateItem item)
        {
            var a = item;
        }


    }
}