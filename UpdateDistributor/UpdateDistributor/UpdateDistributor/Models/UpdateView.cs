﻿using System.Collections.ObjectModel;
using Microsoft.Practices.Prism.ViewModel;

namespace UpdateDistributor.Models
{
    public class UpdateView : NotificationObject
    {
        public string SegmentName { get; set; }
        private ObservableCollection<UpdateItem> _updateItems = new ObservableCollection<UpdateItem>();
        public ObservableCollection<UpdateItem> UpdateItems
        {
            get { return _updateItems; }
            set { if (_updateItems == value) return; _updateItems = value; RaisePropertyChanged(() => UpdateItems); }
        }
    }
}