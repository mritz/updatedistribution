﻿using System.Collections.Generic;
using System.Configuration;
using RCS.Common.Extensions;

namespace UpdateDistributor.Services
{
    public class BaseRestService
    {
        private string _url;
        public BaseRestService()
        {
            _url = ConfigurationManager.AppSettings["UpdateDistributorRestApi"];

        }
        public string BuildUrl(string controller, string method = "")
        {

            var requestUri = string.Format(
                    "{0}/api/{1}{2}",
                    _url,
                    controller,
                    method.IsNullOrWhiteSpace() ? string.Empty : "/" + method);
            return requestUri;
        }
        public Dictionary<string, string> CustomHeaders()
        {
            return new Dictionary<string, string> { { "X-UpdateDistAuthTok", "08F01E0F-140B-4FE4-B493-4A3F2715FC0B" } };
        }
    }
}
