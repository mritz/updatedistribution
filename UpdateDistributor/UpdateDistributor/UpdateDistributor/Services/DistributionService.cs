﻿using System.Collections.Generic;
using System.Linq;
using RCS.Common.Web;
using RFS.DataAccess.BusinessObject.UpdateDistributor;
using RCS.Common.Serialization;

namespace UpdateDistributor.Services
{
    public class DistributionService : BaseRestService
    {
        public IEnumerable<Distribution> GetAll()
        {
            var url = BuildUrl("Distribution");
            var json = WebPost.HttpGet(url, CustomHeaders());
            var distributions = ObjectJsonSerializer<List<Distribution>>.Deserialize(json);
            return distributions;
        }

        public IEnumerable<Distribution> LoadAllActive()
        {
            return GetAll().Where(x=>x.Active);
        }

        public IEnumerable<Distribution> LoadHistoryForParcelId(int parcelId, int environment)
        {
            var distributions = GetAll().Where(x => x.ParcelId == parcelId && x.Environment == environment).ToList();
            distributions.Sort(new HistroyComparer());
            return distributions;
        }

        public DistributionAreas GetDistributionAreas(int parcelId)
        {
            return new DistributionAreas(LoadAllActive(),parcelId);
        }
       
    }

    public class DistributionAreas
    {
        public IEnumerable<Distribution> Dev { get; private set; }
        public IEnumerable<Distribution> QA { get; private set; }
        public IEnumerable<Distribution> Production { get; private set; }

        public DistributionAreas(IEnumerable<Distribution> distributions,int parcelId )
        {
            distributions = distributions.Where(x => x.ParcelId == parcelId);
            Dev = distributions.Where(x => x.Environment == 0);
            QA = distributions.Where(x => x.Environment == 1);
            Production = distributions.Where(x => x.Environment == 2);
        }
    }

    class HistroyComparer : IComparer<Distribution>
    {
        public int Compare(Distribution a, Distribution b)
        {
            if (a.Release < b.Release) return 1;
            if (a.Release == b.Release) return 0;
            return -1;
        }
    }
}