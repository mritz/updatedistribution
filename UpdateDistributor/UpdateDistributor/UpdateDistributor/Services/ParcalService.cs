﻿using System.Collections.Generic;
using RCS.Common.Serialization;
using RCS.Common.Web;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

namespace UpdateDistributor.Services
{
    public class ParcelService : BaseRestService
    {
        public IEnumerable<Parcel> GetAll()
        {
            var url = BuildUrl("Parcel");
            var json = WebPost.HttpGet(url,CustomHeaders());
            return ObjectJsonSerializer<List<Parcel>>.Deserialize(json);
        }
    }
}
