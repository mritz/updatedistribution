﻿using System.Collections.Generic;
using RCS.Common.Serialization;
using RCS.Common.Web;
using RFS.DataAccess.BusinessObject.UpdateDistributor;

namespace UpdateDistributor.Services
{
    public class SegmentService:BaseRestService
    {
        public IEnumerable<Segment> GetAll()
        {
            var url = BuildUrl("Segment");
            var json = WebPost.HttpGet(url,CustomHeaders());
            return ObjectJsonSerializer<List<Segment>>.Deserialize(json);
        }


    }
}
