﻿using System.Collections.Generic;
using System.Linq;
using RFS.DataAccess.BusinessObject.UpdateDistributor;
using UpdateDistributor.Models;

namespace UpdateDistributor.Services
{
    public class UpdateService
    {
        private readonly DistributionService _distributionService;
        private readonly SegmentService _segmentService;
        private readonly ParcelService _parcelService;

        public UpdateService(DistributionService distributionService, SegmentService segmentService, ParcelService parcelService)
        {
            _distributionService = distributionService;
            _segmentService = segmentService;
            _parcelService = parcelService;
        }


        public IEnumerable<UpdateView> GetUpdates()
        {
            var updates = new List<UpdateView>();
            var segments = _segmentService.GetAll();
            var parcels = _parcelService.GetAll();
            var distributions = _distributionService.GetAll();
            foreach (var segment in segments)
            {
                var updateView = new UpdateView
                {
                    SegmentName = segment.Name
                };
                var segmentParcels = parcels.Where(x => x.SegmentId == segment.Id);
                foreach (var parcel in segmentParcels)
                {
                    var parcelDist = distributions.Where(x => x.ParcelId == parcel.Id);
                    var devDist = parcelDist.Where(x => x.Environment == 0).FirstOrDefault(x => x.Active);
                    var qaDist = parcelDist.Where(x => x.Environment == 1).FirstOrDefault(x => x.Active);
                    var prodDist = parcelDist.Where(x => x.Environment == 2).FirstOrDefault(x => x.Active);
                    updateView.UpdateItems.Add(new UpdateItem
                    {
                        ParcelId = parcel.Id,
                        ParcelName = parcel.FileNameOfParcel,
                        DevVer = GetVestion(devDist),
                        QAVer = GetVestion(qaDist),
                        ReleaseVer = GetVestion(prodDist)
                    });
                }
                updates.Add(updateView);
            }
            return updates;
        }

        private string GetVestion(Distribution distribution)
        {
            return distribution == null ? "" : distribution.Version;
        }

    }

}