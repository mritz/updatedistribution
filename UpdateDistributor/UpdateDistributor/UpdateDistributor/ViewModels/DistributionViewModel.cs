﻿using System.Collections.ObjectModel;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.ViewModel;
using UpdateDistributor.Models;
using UpdateDistributor.Services;

namespace UpdateDistributor.ViewModels
{
    public class DistributionViewModel : NotificationObject
    {

        private ObservableCollection<UpdateView> _updates = new ObservableCollection<UpdateView>();
        public ObservableCollection<UpdateView> Updates
        {
            get { return _updates; }
            set { if (_updates == value) return; _updates = value; RaisePropertyChanged(() => Updates); }
        }


        public DistributionViewModel(UpdateService updateService)
        {
            Updates.AddRange(updateService.GetUpdates());
        }

      
    }
}