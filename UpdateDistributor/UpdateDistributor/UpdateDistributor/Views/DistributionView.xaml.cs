﻿using UpdateDistributor.ViewModels;

namespace UpdateDistributor.Views
{
    /// <summary>
    /// Interaction logic for DistributionView.xaml
    /// </summary>
    public partial class DistributionView 
    {
        public DistributionView(DistributionViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
